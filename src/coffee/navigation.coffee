$ ->
	$('#nav a').click (e) ->
	  e.preventDefault()
	  $('html,body').scrollTo @hash, @hash, offset: -40
	  history.pushState(null, null, @hash)


	aChildren = $('#nav li').children()
	# find the a children of the list items
	aArray = []
	# create the empty aArray
	i = 0
	while i < aChildren.length
	  aChild = aChildren[i]
	  ahref = $(aChild).attr('href')
	  aArray.push ahref
	  i++
	
	$(window).scroll ->
	  windowPos = $(window).scrollTop() + 40
	  # get the offset of the window from the top of page
	  windowHeight = $(window).height()
	  # get the height of the window
	  docHeight = $(document).height()

	  if $(window).scrollTop() > 0
	  	$('.header').addClass 'scrolled'
	  else
	  	$('.header').removeClass 'scrolled'
	  	

	  i = 0
	  while i < aArray.length
	    theID = aArray[i]
	    divPos = $(theID).offset().top || null
	    # get the offset of the div from the top of page
	    if divPos
		    divHeight = $(theID).height()
		    # get the height of the div in question
		    if windowPos >= divPos and windowPos < divPos + divHeight
		      $('a[href=\'' + theID + '\']').addClass 'nav-active'
		    else
		      $('a[href=\'' + theID + '\']').removeClass 'nav-active'
		    i++
		  if windowPos + windowHeight == docHeight
		    if !$('#nav li:last-child a').hasClass('nav-active')
		      navActiveCurrent = $('.nav-active').attr('href')
		      $('a[href=\'' + navActiveCurrent + '\']').removeClass 'nav-active'
		      $('#nav li:last-child a').addClass 'nav-active'
	  return