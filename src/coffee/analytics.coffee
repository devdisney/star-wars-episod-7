$ ->

    gtmEvent = (category, action, label) ->
      dataLayer.push
        'event': 'gaEvent'
        'gaEventCategory': category
        'gaEventAction': action
        'gaEventLabel': label
        'gaEventValue': undefined
      return


    setTimeout ( =>
        console.log('Send after 5000 ms')
        gtmEvent('temp', '5 sec', '')
    ), 5000



    $(document).on 'click touchend', "#playBtn", ->
        console.log "send"
        gtmEvent('trailer', 'plaуed', 'TFA 1 Main')
        return

    $(document).on 'click touchend', "#pay-1", ->
        console.log "send"
        gtmEvent('ticket', 'purchased', 'TFA Main')
        return

    $(document).on 'click touchend', "#pay-2", ->
        console.log "send"
        gtmEvent('ticket', 'purchased', 'TFA')
        return

    $(document).on 'click touchend', "#open-trailers", ->
        console.log "send"
        gtmEvent('content', 'viewed', 'TFA Trailers')
        return

    $(document).on 'click touchend', "#open-Stills", ->
        console.log "send"
        gtmEvent('content', 'viewed', 'TFA Stills')
        return

    $(document).on 'click touchend', ".link-an-send", ->
        console.log "send"
        gtmEvent('game', 'installed', 'SW Commander')
        return

    $(document).on 'click touchend', ".trailers__item_send", ->
        index = $(@).data "index"
        console.log "send #{index}"
        gtmEvent('trailer', 'played', "TFA #{index}")
        return

    $(document).on 'click touchend', ".socials--fb", ->
        console.log "send"
        gtmEvent('content', 'shared', 'Facebook')
        return

    $(document).on 'click touchend', ".socials--tw", ->
        console.log "send"
        gtmEvent('content', 'shared', 'Twitter')
        return

    $(document).on 'click touchend', ".socials--ok", ->
        console.log "send"
        gtmEvent('content', 'shared', 'OK')
        return

    $(document).on 'click touchend', '#nav a', ->
        ind = $(@).parents('li').index() + 1
        console.log ind
        gtmEvent('Menu', 'click', 'Position '+ind)

    $(document).on 'click touchend', '.films-carousel a', ->
        ind = $(@).data('index')
        console.log ind
        gtmEvent('Links', 'click', 'SW Movie ' + ind)


    $(document).on 'click touchend', '.foo-nav a', ->
        ind = $(@).parents('li').index() + 1
        console.log ind
        gtmEvent('Footer', 'click', 'Position ' + ind)

    $(document).on 'click touchend', '.baner a', ->
        gtmEvent('Links', 'click', 'TFA Contest')


    
    $(document).on 'click touchend', '.soundtrack-send', ->
        gtmEvent('music', 'purchased', 'SW 7 Soundtrack')


    
    $(document).on 'click touchend', '.ringtones-send', ->
        gtmEvent('music', 'purchased', 'SW 7 Beeline Privet')


    