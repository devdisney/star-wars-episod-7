setHeightCalc = (el, r) ->
    $(el).each ->
        $this = $(@)
        c = $(@).data 'c'
        etalon = $(@).find('.etalon')


        $this.addClass 'ready'

        set = () ->
            setTimeout ( =>
                h = Math.floor(etalon.outerHeight(true) * c) - 2
                $this.data('etalon', h)
                $this.css
                    height: h
            ), 100

        if r == 'big'
            if $(window).width() > 490
                set()
            else
                $this.removeAttr 'style'

        else if r == 'small'
            if $(window).width() < 490
                set()
            else
                $this.removeAttr 'style'
        else if r == 'always'
            set()


$(window).load ->
    isImageOk = (img) ->
        
        if !img.complete
            return false
        
        if typeof img.naturalWidth != "undefined" && img.naturalWidth == 0
            return false

        setHeightCalc('.js-max-height', 'always');
        setHeightCalc('.js-max-height-calculation', 'big');
        return true



    $('.js-max-height .etalon img, .js-max-height-calculation .etalon img').each ->
        isImageOk @

$ ->
    events = {
        evClick: (el) ->
            target = $(el).data 'expand'
            to = $(el).data 'to'

            if $(target).data('etalon') > 0
                hTr = $(target).data('etalon')
            else
                hTr = 0

            $(el)
                .addClass 'hide'
                .siblings()
                .removeClass 'hide'

            $(target).toggleClass 'open'

            if to == 'down'
                $(target)
                    .stop(true)
                    .animate
                        height: $(target).get(0).scrollHeight
                        500
            else if to == 'up'
                parent = $(target).parents('.section').attr('id')
                $(target)
                    .stop(true)
                    .animate
                        height: hTr
                        500         
                $('html,body').scrollTo '#'+parent, '#'+parent
    }

    setSrc = () ->
        $('.frames__item img').each ->
            src = $(@).data 'src'
            $(@).attr('src', src) 


    $(document)
        .on 'click', '[data-expand]', ->
            # removeSrc()
            setSrc()
            events.evClick($(@))
            return
        .on 'touchstart', '[data-expand]', (e) ->
            oldScrollTop = $(window).scrollTop()
            $this = $(@)
            setTimeout ( =>
                newScrollTop = $(window).scrollTop()
                if Math.abs(oldScrollTop-newScrollTop) < 3
                    events.evClick($this)
            ), 100








    $(window).on 'resize', ->
        setHeightCalc('.js-max-height', 'always');
        setHeightCalc('.js-max-height-calculation', 'big')
        setHeightCalc('.handmade-items', 'small');
        $('[data-to="down"]').removeClass 'hide'
        $('[data-to="up"]').addClass 'hide'
