$ ->

    keyControl = (el) ->
        $(document).on 'keyup', (e) ->
            if e.which == 39
                type = 'next'
            else if e.which == 37
                type = 'prev'
            el.trigger(type+'.owl.carousel')

    keyControl($('.keycontrol'))


    # js-click
    $(document).on 'click', '.iOS .js-click', ->
        href = $(@).attr 'href'
        window.location = href


    delay = (ms, func) -> setTimeout func, ms

    isInArray = (value, array) ->
      array.indexOf(value) > -1


    heroesArray = []
    framesArray = []
    hash = document.location.hash

    $('[data-carousel-popup-target="#frames-popup"]').each ->
        framesArray.push('frame-' + ($(@).data('index')))
    
    $('.carousel-heroes .item').each ->
        heroesArray.push($(@).data('hash'))

    if isInArray(hash.substring(1), heroesArray)
        delay 4000, ->
            $('html,body').scrollTo '#heroes', '#heroes', offset: -40

    if isInArray(hash.substring(1), framesArray)
        delay 3000, ->
            $('html,body').scrollTo '#frames', '#frames', offset: -40
            $('[data-carousel-popup-target]').eq(hash.substring(7)-1).trigger('click')