$ ->
	md = new MobileDetect(window.navigator.userAgent)

	if md.os() == 'AndroidOS'
		$('body').addClass 'AndroidOS notiOS'
	else if md.os() == 'iOS'
		$('body').addClass 'iOS notAndroidOS'

	if navigator.userAgent.match(/Windows Phone OS|Windows CE|Windows Mobile|IEMobile|Windows Phone OS 7|Windows Phone 8/i)
		$('body').addClass 'winphones'

	if md.mobile()
		$('body').addClass 'mobile'