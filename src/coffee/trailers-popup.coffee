+ do ($ = jQuery, window) ->
    'use strict';
    class TrailerPopup
        constructor: (element) ->
            @el = element
            @trailerContainer = $('#trailer')

        showPopup: ->
            $(@el).addClass 'show'

        hidePopup: ->
            $(@el).removeClass 'show'

        setSrc: ->
            src = $(@el).data 'src'
            $(@trailerContainer).attr('src', src) 

    Plugin = (option, value) ->
        @.each ->
            $this = $(@)
            data = $this.data('cl.trailerpopup')

            if !data 
                $this.data('cl.trailerpopup', (data = new TrailerPopup @))
            if (typeof option == 'string')
                data[option]()
            return
    
    $ ->

        events = {
            evClick: (el) ->
                $this = $(el)
                target = $this.data 'popup-target'

                Plugin.call($this, 'setSrc')
                Plugin.call($(target), 'showPopup')
        }

        $(document)
            .on 'click', '[data-popup-target]', (e)->
                e.preventDefault()
                $this = $(@)
                events.evClick($this)
            .on 'touchstart', '[data-popup-target]', (e) ->
                oldScrollTop = $(window).scrollTop()
                $this = $(@)
                setTimeout ( =>
                    newScrollTop = $(window).scrollTop()
                    if Math.abs(oldScrollTop-newScrollTop) < 3
                        events.evClick($this)
                ), 200

        $(document).on 'click', '.overlay', ->
            Plugin.call($('.popup'), 'hidePopup')
            $('#trailer').attr('src', '')
        
        $(document).on 'click', '.js-close', ->
            Plugin.call($('.popup'), 'hidePopup')
            $('#trailer').attr('src', '')