playerYoutube = undefined
# embedId = '1lpeaehiu8'
# embedId = 'c2ouy4z96t'
# videoId = 'LXsdBfD1WNw'

onYouTubeIframeAPIReady = ->
  playerYoutube = new (YT.Player)('my-video-container',
    height: '100%'
    width: '100%'
    playerVars:
       'autoplay': 0
       'controls': 1
       'autohide':1
       'wmode':'opaque'
       'showinfo':0
    events: 
      'onReady': (e) ->
    videoId: videoId)

playYVideo = ->
  playerYoutube.playVideo()

stopYVideo = ->
  playerYoutube.stopVideo()


$ ->
  globalVideoContainer = $('#video_container')
  wistiaContainer = $('.wistia_embed')
  youtubeContainer = $('#y-video')
  closeYoutubeContainer = $('#closeYoutubeVideo')
  playBtn = $('#playBtn')
  viewVideos = $('#view-videos')
  flagSCroll = true
  events = {}
  endPoint = 800
  
  playYoutubeEmbed = ->
    playYVideo()
    flagSCroll = false
    return

  stopYoutubeEmbed = ->
    stopYVideo()
    flagSCroll = true
    return

  setParams = (w, h) ->
    if w >= endPoint
      globalVideoContainer.css
        height: h
    else
      globalVideoContainer.css
        height: 100 + '%'
    return


  if window.preview.state == 'video'
    
    wistiaEmbed = undefined
    
    videoOptions = 
      plugin: {}
      volume: 0
      videoFoam: true
      autoPlay: true

    embedVideo = do ->

      init = (options, w) ->
        if w <= endPoint
          options.plugin = {}
        else
          options.plugin = cropFill: src: 'http://fast.wistia.com/labs/crop-fill/plugin.js'
        wistiaEmbed = Wistia.embed(embedId, options)
        wistiaEmbed.bind 'play', ->
          wistiaEmbed.pause()
          wistiaEmbed.time 0
          wistiaEmbed.play()
          @unbind
        wistiaEmbed.bind 'end', ->
          wistiaEmbed.play()
          return
        return

      init videoOptions, $(window).width()
      
      $(window).on 'resize', ->
        Wistia.rebind()
        init videoOptions, $(window).width()
        return
      return

    playWistiaVideo = ->
      wistiaEmbed.play()
      return

    stopWistiaVideo = ->
      wistiaEmbed.pause()
      return

    turnOff = ->
      $(window).on 'scroll', ->
        $this = $(this)
        if flagSCroll == true
          h = globalVideoContainer.height()
          wh = $this.scrollTop() + $this.height() / 2
          if h < wh
            stopWistiaVideo()
          else
            playWistiaVideo()
        return
      return

    setParams $(window).width(), $(window).height()
    turnOff()

    events = {
      evPlay: ->
        stopWistiaVideo()
        wistiaContainer.hide()
        youtubeContainer.addClass 'show'
        closeYoutubeContainer.addClass 'show'
        playYoutubeEmbed()

      evClose: (el) ->
        $(el).removeClass 'show'
        stopYoutubeEmbed()
        wistiaContainer.show()
        youtubeContainer.removeClass 'show'
        playWistiaVideo()
    }

    $(window).on 'resize orientationchange', ->
      turnOff()


  if window.preview.state == 'image'
    events = {
      evPlay: ->
        youtubeContainer.addClass 'show'
        closeYoutubeContainer.addClass 'show'
        playYoutubeEmbed()

      evClose: (el) ->
        $(el).removeClass 'show'
        stopYoutubeEmbed()
        youtubeContainer.removeClass 'show'
    }

  $(document)
    .on 'click touchstart', '#playBtn', ->
        events.evPlay()

  $(document)
    .on 'click touchstart', '#closeYoutubeVideo', ->
      events.evClose($(@))
  
  $(window).on 'resize orientationchange', ->
    setParams $(@).width(), $(@).height()
  return