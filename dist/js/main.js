$(function() {
  var gtmEvent;
  gtmEvent = function(category, action, label) {
    dataLayer.push({
      'event': 'gaEvent',
      'gaEventCategory': category,
      'gaEventAction': action,
      'gaEventLabel': label,
      'gaEventValue': void 0
    });
  };
  setTimeout(((function(_this) {
    return function() {
      console.log('Send after 5000 ms');
      return gtmEvent('temp', '5 sec', '');
    };
  })(this)), 5000);
  $(document).on('click touchend', "#playBtn", function() {
    console.log("send");
    gtmEvent('trailer', 'plaуed', 'TFA 1 Main');
  });
  $(document).on('click touchend', "#pay-1", function() {
    console.log("send");
    gtmEvent('ticket', 'purchased', 'TFA Main');
  });
  $(document).on('click touchend', "#pay-2", function() {
    console.log("send");
    gtmEvent('ticket', 'purchased', 'TFA');
  });
  $(document).on('click touchend', "#open-trailers", function() {
    console.log("send");
    gtmEvent('content', 'viewed', 'TFA Trailers');
  });
  $(document).on('click touchend', "#open-Stills", function() {
    console.log("send");
    gtmEvent('content', 'viewed', 'TFA Stills');
  });
  $(document).on('click touchend', ".link-an-send", function() {
    console.log("send");
    gtmEvent('game', 'installed', 'SW Commander');
  });
  $(document).on('click touchend', ".trailers__item_send", function() {
    var index;
    index = $(this).data("index");
    console.log("send " + index);
    gtmEvent('trailer', 'played', "TFA " + index);
  });
  $(document).on('click touchend', ".socials--fb", function() {
    console.log("send");
    gtmEvent('content', 'shared', 'Facebook');
  });
  $(document).on('click touchend', ".socials--tw", function() {
    console.log("send");
    gtmEvent('content', 'shared', 'Twitter');
  });
  $(document).on('click touchend', ".socials--ok", function() {
    console.log("send");
    gtmEvent('content', 'shared', 'OK');
  });
  $(document).on('click touchend', '#nav a', function() {
    var ind;
    ind = $(this).parents('li').index() + 1;
    console.log(ind);
    return gtmEvent('Menu', 'click', 'Position ' + ind);
  });
  $(document).on('click touchend', '.films-carousel a', function() {
    var ind;
    ind = $(this).data('index');
    console.log(ind);
    return gtmEvent('Links', 'click', 'SW Movie ' + ind);
  });
  $(document).on('click touchend', '.foo-nav a', function() {
    var ind;
    ind = $(this).parents('li').index() + 1;
    console.log(ind);
    return gtmEvent('Footer', 'click', 'Position ' + ind);
  });
  $(document).on('click touchend', '.baner a', function() {
    return gtmEvent('Links', 'click', 'TFA Contest');
  });
  $(document).on('click touchend', '.soundtrack-send', function() {
    return gtmEvent('music', 'purchased', 'SW 7 Soundtrack');
  });
  return $(document).on('click touchend', '.ringtones-send', function() {
    return gtmEvent('music', 'purchased', 'SW 7 Beeline Privet');
  });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFuYWx5dGljcy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsQ0FBQSxDQUFFLFNBQUE7QUFFRSxNQUFBO0VBQUEsUUFBQSxHQUFXLFNBQUMsUUFBRCxFQUFXLE1BQVgsRUFBbUIsS0FBbkI7SUFDVCxTQUFTLENBQUMsSUFBVixDQUNFO01BQUEsT0FBQSxFQUFTLFNBQVQ7TUFDQSxpQkFBQSxFQUFtQixRQURuQjtNQUVBLGVBQUEsRUFBaUIsTUFGakI7TUFHQSxjQUFBLEVBQWdCLEtBSGhCO01BSUEsY0FBQSxFQUFnQixNQUpoQjtLQURGO0VBRFM7RUFVWCxVQUFBLENBQVcsQ0FBRSxDQUFBLFNBQUEsS0FBQTtXQUFBLFNBQUE7TUFDVCxPQUFPLENBQUMsR0FBUixDQUFZLG9CQUFaO2FBQ0EsUUFBQSxDQUFTLE1BQVQsRUFBaUIsT0FBakIsRUFBMEIsRUFBMUI7SUFGUztFQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBRixDQUFYLEVBR0csSUFISDtFQU9BLENBQUEsQ0FBRSxRQUFGLENBQVcsQ0FBQyxFQUFaLENBQWUsZ0JBQWYsRUFBaUMsVUFBakMsRUFBNkMsU0FBQTtJQUN6QyxPQUFPLENBQUMsR0FBUixDQUFZLE1BQVo7SUFDQSxRQUFBLENBQVMsU0FBVCxFQUFvQixRQUFwQixFQUE4QixZQUE5QjtFQUZ5QyxDQUE3QztFQUtBLENBQUEsQ0FBRSxRQUFGLENBQVcsQ0FBQyxFQUFaLENBQWUsZ0JBQWYsRUFBaUMsUUFBakMsRUFBMkMsU0FBQTtJQUN2QyxPQUFPLENBQUMsR0FBUixDQUFZLE1BQVo7SUFDQSxRQUFBLENBQVMsUUFBVCxFQUFtQixXQUFuQixFQUFnQyxVQUFoQztFQUZ1QyxDQUEzQztFQUtBLENBQUEsQ0FBRSxRQUFGLENBQVcsQ0FBQyxFQUFaLENBQWUsZ0JBQWYsRUFBaUMsUUFBakMsRUFBMkMsU0FBQTtJQUN2QyxPQUFPLENBQUMsR0FBUixDQUFZLE1BQVo7SUFDQSxRQUFBLENBQVMsUUFBVCxFQUFtQixXQUFuQixFQUFnQyxLQUFoQztFQUZ1QyxDQUEzQztFQUtBLENBQUEsQ0FBRSxRQUFGLENBQVcsQ0FBQyxFQUFaLENBQWUsZ0JBQWYsRUFBaUMsZ0JBQWpDLEVBQW1ELFNBQUE7SUFDL0MsT0FBTyxDQUFDLEdBQVIsQ0FBWSxNQUFaO0lBQ0EsUUFBQSxDQUFTLFNBQVQsRUFBb0IsUUFBcEIsRUFBOEIsY0FBOUI7RUFGK0MsQ0FBbkQ7RUFLQSxDQUFBLENBQUUsUUFBRixDQUFXLENBQUMsRUFBWixDQUFlLGdCQUFmLEVBQWlDLGNBQWpDLEVBQWlELFNBQUE7SUFDN0MsT0FBTyxDQUFDLEdBQVIsQ0FBWSxNQUFaO0lBQ0EsUUFBQSxDQUFTLFNBQVQsRUFBb0IsUUFBcEIsRUFBOEIsWUFBOUI7RUFGNkMsQ0FBakQ7RUFLQSxDQUFBLENBQUUsUUFBRixDQUFXLENBQUMsRUFBWixDQUFlLGdCQUFmLEVBQWlDLGVBQWpDLEVBQWtELFNBQUE7SUFDOUMsT0FBTyxDQUFDLEdBQVIsQ0FBWSxNQUFaO0lBQ0EsUUFBQSxDQUFTLE1BQVQsRUFBaUIsV0FBakIsRUFBOEIsY0FBOUI7RUFGOEMsQ0FBbEQ7RUFLQSxDQUFBLENBQUUsUUFBRixDQUFXLENBQUMsRUFBWixDQUFlLGdCQUFmLEVBQWlDLHNCQUFqQyxFQUF5RCxTQUFBO0FBQ3JELFFBQUE7SUFBQSxLQUFBLEdBQVEsQ0FBQSxDQUFFLElBQUYsQ0FBSSxDQUFDLElBQUwsQ0FBVSxPQUFWO0lBQ1IsT0FBTyxDQUFDLEdBQVIsQ0FBWSxPQUFBLEdBQVEsS0FBcEI7SUFDQSxRQUFBLENBQVMsU0FBVCxFQUFvQixRQUFwQixFQUE4QixNQUFBLEdBQU8sS0FBckM7RUFIcUQsQ0FBekQ7RUFNQSxDQUFBLENBQUUsUUFBRixDQUFXLENBQUMsRUFBWixDQUFlLGdCQUFmLEVBQWlDLGNBQWpDLEVBQWlELFNBQUE7SUFDN0MsT0FBTyxDQUFDLEdBQVIsQ0FBWSxNQUFaO0lBQ0EsUUFBQSxDQUFTLFNBQVQsRUFBb0IsUUFBcEIsRUFBOEIsVUFBOUI7RUFGNkMsQ0FBakQ7RUFLQSxDQUFBLENBQUUsUUFBRixDQUFXLENBQUMsRUFBWixDQUFlLGdCQUFmLEVBQWlDLGNBQWpDLEVBQWlELFNBQUE7SUFDN0MsT0FBTyxDQUFDLEdBQVIsQ0FBWSxNQUFaO0lBQ0EsUUFBQSxDQUFTLFNBQVQsRUFBb0IsUUFBcEIsRUFBOEIsU0FBOUI7RUFGNkMsQ0FBakQ7RUFLQSxDQUFBLENBQUUsUUFBRixDQUFXLENBQUMsRUFBWixDQUFlLGdCQUFmLEVBQWlDLGNBQWpDLEVBQWlELFNBQUE7SUFDN0MsT0FBTyxDQUFDLEdBQVIsQ0FBWSxNQUFaO0lBQ0EsUUFBQSxDQUFTLFNBQVQsRUFBb0IsUUFBcEIsRUFBOEIsSUFBOUI7RUFGNkMsQ0FBakQ7RUFLQSxDQUFBLENBQUUsUUFBRixDQUFXLENBQUMsRUFBWixDQUFlLGdCQUFmLEVBQWlDLFFBQWpDLEVBQTJDLFNBQUE7QUFDdkMsUUFBQTtJQUFBLEdBQUEsR0FBTSxDQUFBLENBQUUsSUFBRixDQUFJLENBQUMsT0FBTCxDQUFhLElBQWIsQ0FBa0IsQ0FBQyxLQUFuQixDQUFBLENBQUEsR0FBNkI7SUFDbkMsT0FBTyxDQUFDLEdBQVIsQ0FBWSxHQUFaO1dBQ0EsUUFBQSxDQUFTLE1BQVQsRUFBaUIsT0FBakIsRUFBMEIsV0FBQSxHQUFZLEdBQXRDO0VBSHVDLENBQTNDO0VBS0EsQ0FBQSxDQUFFLFFBQUYsQ0FBVyxDQUFDLEVBQVosQ0FBZSxnQkFBZixFQUFpQyxtQkFBakMsRUFBc0QsU0FBQTtBQUNsRCxRQUFBO0lBQUEsR0FBQSxHQUFNLENBQUEsQ0FBRSxJQUFGLENBQUksQ0FBQyxJQUFMLENBQVUsT0FBVjtJQUNOLE9BQU8sQ0FBQyxHQUFSLENBQVksR0FBWjtXQUNBLFFBQUEsQ0FBUyxPQUFULEVBQWtCLE9BQWxCLEVBQTJCLFdBQUEsR0FBYyxHQUF6QztFQUhrRCxDQUF0RDtFQU1BLENBQUEsQ0FBRSxRQUFGLENBQVcsQ0FBQyxFQUFaLENBQWUsZ0JBQWYsRUFBaUMsWUFBakMsRUFBK0MsU0FBQTtBQUMzQyxRQUFBO0lBQUEsR0FBQSxHQUFNLENBQUEsQ0FBRSxJQUFGLENBQUksQ0FBQyxPQUFMLENBQWEsSUFBYixDQUFrQixDQUFDLEtBQW5CLENBQUEsQ0FBQSxHQUE2QjtJQUNuQyxPQUFPLENBQUMsR0FBUixDQUFZLEdBQVo7V0FDQSxRQUFBLENBQVMsUUFBVCxFQUFtQixPQUFuQixFQUE0QixXQUFBLEdBQWMsR0FBMUM7RUFIMkMsQ0FBL0M7RUFLQSxDQUFBLENBQUUsUUFBRixDQUFXLENBQUMsRUFBWixDQUFlLGdCQUFmLEVBQWlDLFVBQWpDLEVBQTZDLFNBQUE7V0FDekMsUUFBQSxDQUFTLE9BQVQsRUFBa0IsT0FBbEIsRUFBMkIsYUFBM0I7RUFEeUMsQ0FBN0M7RUFLQSxDQUFBLENBQUUsUUFBRixDQUFXLENBQUMsRUFBWixDQUFlLGdCQUFmLEVBQWlDLGtCQUFqQyxFQUFxRCxTQUFBO1dBQ2pELFFBQUEsQ0FBUyxPQUFULEVBQWtCLFdBQWxCLEVBQStCLGlCQUEvQjtFQURpRCxDQUFyRDtTQUtBLENBQUEsQ0FBRSxRQUFGLENBQVcsQ0FBQyxFQUFaLENBQWUsZ0JBQWYsRUFBaUMsaUJBQWpDLEVBQW9ELFNBQUE7V0FDaEQsUUFBQSxDQUFTLE9BQVQsRUFBa0IsV0FBbEIsRUFBK0IscUJBQS9CO0VBRGdELENBQXBEO0FBaEdGLENBQUYiLCJmaWxlIjoiYW5hbHl0aWNzLmpzIn0=


/* 
    Init owlCarousel with custom DOM options
    
    http://www.owlcarousel.owlgraphic.com/docs/api-options.html
    @PluginCreateCarousel-options 
    @responsive-options
    @nav-text
   
    https://github.com/smashingboxes/OwlCarousel2
 */
+(function($, window) {
  'use strict';
  var CarouselPopup, CreateCarousel, PluginCarouselPopup, PluginCreateCarousel;
  CreateCarousel = (function() {
    var prepareResponseOption;

    function CreateCarousel(element) {
      this.el = $(element);
      this.options = {
        loop: false,
        nav: false,
        responsive: {},
        navText: [],
        freeDrag: false,
        startPosition: 'URLHash',
        URLhashListener: true
      };
    }

    CreateCarousel.prototype.build = function() {
      var $this, opt;
      $this = this.el;
      opt = this.extendOptions();
      if (typeof $.fn.owlCarousel === 'function') {
        return $this.owlCarousel(opt);
      } else {
        return console.log('You must install owlCarousel https://github.com/smashingboxes/OwlCarousel2');
      }
    };

    CreateCarousel.prototype.getOptions = function() {
      var $this, optionsObj;
      $this = this.el;
      return optionsObj = {
        domOptions: $this.data('plugin-options'),
        responsiveDomOptions: $this.data('responsive-options'),
        navText: $this.data('nav-text')
      };
    };

    CreateCarousel.prototype.extendOptions = function() {
      var $options, $optionsObj, $this;
      $this = this.el;
      $options = this.options;
      $optionsObj = this.getOptions();
      if (($optionsObj != null) && Object.keys($optionsObj).length !== 0) {
        $.extend($options, $optionsObj.domOptions);
      }
      if (($optionsObj.responsiveDomOptions != null) && Object.keys($optionsObj.responsiveDomOptions).length !== 0) {
        $.extend(true, $options.responsive, $optionsObj.responsiveDomOptions);
      }
      if ($optionsObj.navText != null) {
        $.extend(true, $options.navText, $optionsObj.navText.split(','));
      }
      return $options;
    };

    CreateCarousel.prototype.destroy = function() {
      return $(this.el).trigger('destroy.owl.carousel');
    };

    prepareResponseOption = function(opts) {
      var key, value;
      for (key in opts) {
        value = opts[key];
        opts[key] = {
          items: value
        };
      }
      return opts;
    };

    return CreateCarousel;

  })();
  CarouselPopup = (function() {
    function CarouselPopup(element) {
      this.el = element;
    }

    CarouselPopup.prototype.showPopup = function() {
      return $(this.el).addClass('show');
    };

    CarouselPopup.prototype.hidePopup = function() {
      return $(this.el).removeClass('show');
    };

    CarouselPopup.prototype.goTo = function() {
      var $el, owl, thumb;
      $el = $(this.el);
      owl = $el.data('carousel');
      thumb = $el.data('thumb');
      return $(owl).trigger('to.owl.carousel', parseInt(thumb - 1));
    };

    CarouselPopup.prototype.goFromPage = function() {
      var $el, index, popup;
      $el = $(this.el);
      popup = $el.data('carousel-popup-target');
      index = $el.data('index');
      return $(popup).find('#screens').trigger('to.owl.carousel', index - 2).trigger('to.owl.carousel', index - 1).on('translated.owl.carousel', function() {
        $(window).resize();
        return console.log('refreshed');
      });
    };

    return CarouselPopup;

  })();
  PluginCreateCarousel = function(option) {
    return this.each(function() {
      var $this, data;
      $this = $(this);
      data = $this.data('cl.createcarousel');
      if (!data) {
        $this.data('cl.createcarousel', (data = new CreateCarousel(this)));
      }
      if (typeof option === 'string') {
        data[option]();
      }
    });
  };
  PluginCarouselPopup = function(option, value) {
    return this.each(function() {
      var $this, data;
      $this = $(this);
      data = $this.data('cl.CarouselPopup');
      if (!data) {
        $this.data('cl.CarouselPopup', (data = new CarouselPopup(this)));
      }
      if (typeof option === 'string') {
        data[option]();
      }
    });
  };
  $(function() {
    var destroyCarousel, duration, flag, gtmEvent, initSmCarousel, screens, sendGa, setActiveClass, thumbs;
    initSmCarousel = function(w, tr, $this) {
      if (w < tr) {
        PluginCreateCarousel.call($this, 'build');
      } else {
        PluginCreateCarousel.call($this, 'destroy');
      }
    };
    destroyCarousel = function(w, tr, $this) {
      if (w > tr) {
        PluginCreateCarousel.call($this, 'build');
      } else {
        PluginCreateCarousel.call($this, 'destroy');
      }
    };
    $('.carousel').each(function() {
      var $this, tr;
      $this = $(this);
      tr = $this.data('destroy');
      if (tr) {
        destroyCarousel($(window).width(), tr, $this);
        $(window).on('resize', function() {
          return destroyCarousel($(this).width(), tr, $this);
        });
      } else {
        PluginCreateCarousel.call($this, 'build');
      }
    });
    $('.must-be-carousel').each(function() {
      var $this, tr;
      $this = $(this);
      tr = $this.data('threshold');
      initSmCarousel($(window).width(), tr, $this);
      return $(window).on('resize', function() {
        return initSmCarousel($(this).width(), tr, $this);
      });
    });
    $('#carousel-heroes').on('changed.owl.carousel', function(event) {
      return console.log(event.item);
    });
    gtmEvent = function(category, action, label) {
      dataLayer.push({
        'event': 'gaEvent',
        'gaEventCategory': category,
        'gaEventAction': action,
        'gaEventLabel': label,
        'gaEventValue': void 0
      });
    };
    sendGa = function(index, string) {
      console.log(string + " " + index);
      return gtmEvent('Content', 'viewed', string + " " + index);
    };
    $('#carousel-heroes, #frames-carousel').on('translated.owl.carousel', function(e) {
      var index, string;
      string = $(this).data('string');
      index = e.page.index + 1;
      return sendGa(index, string);
    });
    $('#screens').on('translate.owl.carousel', function(e) {
      var index, string;
      string = $(this).data('string');
      index = e.page.index + 1;
      return sendGa(index, string);
    });
    $('#first-frame').on('touchend', function(e) {
      var ev, index, string;
      string = 'Dinosaur Still';
      index = 1;
      ev = $(this).data('max-event');
      if ($(window).width() > ev) {
        return sendGa(index, string);
      }
    });
    screens = $('#screens');
    thumbs = $('#thumbs');
    screens.on('translate.owl.carousel', function() {
      console.log('start', $(this));
      return $(this).find('.owl-stage').css({
        visibility: 'hidden'
      });
    });
    screens.on('translated.owl.carousel', function() {
      console.log('end', $(this));
      return $(this).find('.owl-stage').css({
        visibility: 'visible'
      });
    });
    $('.screens img').each(function() {
      var h, offset;
      h = $(window).height();
      offset = 220;
      $(this).css('max-height', h - offset);
      return $('.screens .owl-stage-outer').css('max-height', h - offset).css('height', $('.screens').height());
    });
    $(window).on('resize', function() {
      var h;
      h = $(window).height();
      return $('.screens img').each(function() {
        var offset;
        offset = 220;
        $(this).css('max-height', h - offset);
        return $('.screens .owl-stage-outer').css('max-height', h - offset).css('height', $('.screens').height());
      });
    });
    $(document).on('click', '[data-carousel-popup-target]', function() {
      var $this, carousel, max, target;
      max = $(this).data('max-event');
      if ($(window).width() >= max) {
        $this = $(this);
        target = $this.data('carousel-popup-target');
        carousel = $(target).find('.carousel');
        carousel.each(function() {
          $(this).find('.v img').each(function() {
            var src;
            src = $(this).data('src');
            return $(this).attr('src', src);
          });
          PluginCreateCarousel.call($(this), 'destroy');
          PluginCreateCarousel.call($(this), 'build');
          return $(this).trigger('refresh.owl.carousel');
        });
        PluginCarouselPopup.call($(target), 'showPopup');
        return PluginCarouselPopup.call($this, 'goFromPage');
      }
    });
    $(document).on('click', '.overlay', function() {
      return PluginCarouselPopup.call($('.popup'), 'hidePopup');
    });
    $(document).on('click', '.js-close', function() {
      return PluginCarouselPopup.call($('.popup'), 'hidePopup');
    });
    $(document).on('click', '[data-thumb]', function() {
      return PluginCarouselPopup.call($(this), 'goTo');
    });
    flag = false;
    duration = 300;
    setActiveClass = function(i) {
      $('[data-thumb]').removeClass('active');
      return $('[data-thumb="' + i + '"]').addClass('active');
    };
    screens.on('changed.owl.carousel', function(e) {
      if (!flag) {
        flag = true;
        thumbs.trigger('to.owl.carousel', [e.item.index, duration, true]);
        setActiveClass(e.item.index + 1);
        return flag = false;
      }
    });
    return $('#frames-carousel').on('changed.owl.carousel', function(e) {
      return $('#counter').text((e.item.index + 1) + " из " + e.item.count);
    });
  });
})(jQuery, window);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNhcm91c2VsLmNsYXNzLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7Ozs7Ozs7Ozs7QUFVQSxDQUFLLENBQUEsU0FBQyxDQUFELEVBQWEsTUFBYjtFQUNEO0FBQUEsTUFBQTtFQUNNO0FBQ0YsUUFBQTs7SUFBYSx3QkFBQyxPQUFEO01BQ1QsSUFBQyxDQUFDLEVBQUYsR0FBTyxDQUFBLENBQUUsT0FBRjtNQUNQLElBQUMsQ0FBQyxPQUFGLEdBQVk7UUFDUixJQUFBLEVBQU0sS0FERTtRQUVSLEdBQUEsRUFBSyxLQUZHO1FBR1IsVUFBQSxFQUFZLEVBSEo7UUFJUixPQUFBLEVBQVMsRUFKRDtRQUtSLFFBQUEsRUFBVSxLQUxGO1FBTVIsYUFBQSxFQUFlLFNBTlA7UUFPUixlQUFBLEVBQWdCLElBUFI7O0lBRkg7OzZCQVliLEtBQUEsR0FBTyxTQUFBO0FBQ0gsVUFBQTtNQUFBLEtBQUEsR0FBUSxJQUFDLENBQUM7TUFDVixHQUFBLEdBQU0sSUFBQyxDQUFDLGFBQUYsQ0FBQTtNQUVOLElBQUcsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLFdBQVosS0FBMkIsVUFBOUI7ZUFDSSxLQUFLLENBQUMsV0FBTixDQUFrQixHQUFsQixFQURKO09BQUEsTUFBQTtlQUdJLE9BQU8sQ0FBQyxHQUFSLENBQVksNEVBQVosRUFISjs7SUFKRzs7NkJBU1AsVUFBQSxHQUFZLFNBQUE7QUFDUixVQUFBO01BQUEsS0FBQSxHQUFRLElBQUMsQ0FBQzthQUNWLFVBQUEsR0FBYTtRQUNULFVBQUEsRUFBWSxLQUFLLENBQUMsSUFBTixDQUFXLGdCQUFYLENBREg7UUFFVCxvQkFBQSxFQUFzQixLQUFLLENBQUMsSUFBTixDQUFXLG9CQUFYLENBRmI7UUFHVCxPQUFBLEVBQVMsS0FBSyxDQUFDLElBQU4sQ0FBVyxVQUFYLENBSEE7O0lBRkw7OzZCQVNaLGFBQUEsR0FBZSxTQUFBO0FBQ1gsVUFBQTtNQUFBLEtBQUEsR0FBUSxJQUFDLENBQUM7TUFDVixRQUFBLEdBQVcsSUFBQyxDQUFDO01BQ2IsV0FBQSxHQUFjLElBQUMsQ0FBQyxVQUFGLENBQUE7TUFFZCxJQUFHLHFCQUFBLElBQWlCLE1BQU0sQ0FBQyxJQUFQLENBQVksV0FBWixDQUF3QixDQUFDLE1BQXpCLEtBQW1DLENBQXZEO1FBQ0ksQ0FBQyxDQUFDLE1BQUYsQ0FBUyxRQUFULEVBQW1CLFdBQVcsQ0FBQyxVQUEvQixFQURKOztNQUdBLElBQUksMENBQUEsSUFBc0MsTUFBTSxDQUFDLElBQVAsQ0FBWSxXQUFXLENBQUMsb0JBQXhCLENBQTZDLENBQUMsTUFBOUMsS0FBd0QsQ0FBbEc7UUFHSSxDQUFDLENBQUMsTUFBRixDQUFTLElBQVQsRUFBZSxRQUFRLENBQUMsVUFBeEIsRUFBb0MsV0FBVyxDQUFDLG9CQUFoRCxFQUhKOztNQUtBLElBQUcsMkJBQUg7UUFDSSxDQUFDLENBQUMsTUFBRixDQUFTLElBQVQsRUFBZSxRQUFRLENBQUMsT0FBeEIsRUFBaUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxLQUFwQixDQUEwQixHQUExQixDQUFqQyxFQURKOztBQUdBLGFBQU87SUFoQkk7OzZCQWtCZixPQUFBLEdBQVMsU0FBQTthQUNMLENBQUEsQ0FBRSxJQUFDLENBQUEsRUFBSCxDQUFNLENBQUMsT0FBUCxDQUFlLHNCQUFmO0lBREs7O0lBR1QscUJBQUEsR0FBd0IsU0FBQyxJQUFEO0FBQ3BCLFVBQUE7QUFBQSxXQUFBLFdBQUE7O1FBQ0UsSUFBSyxDQUFBLEdBQUEsQ0FBTCxHQUFZO1VBQ1YsS0FBQSxFQUFPLEtBREc7O0FBRGQ7QUFLQSxhQUFPO0lBTmE7Ozs7O0VBU3RCO0lBQ1csdUJBQUMsT0FBRDtNQUNULElBQUMsQ0FBQSxFQUFELEdBQU07SUFERzs7NEJBR2IsU0FBQSxHQUFXLFNBQUE7YUFDUCxDQUFBLENBQUUsSUFBQyxDQUFBLEVBQUgsQ0FBTSxDQUFDLFFBQVAsQ0FBZ0IsTUFBaEI7SUFETzs7NEJBR1gsU0FBQSxHQUFXLFNBQUE7YUFDUCxDQUFBLENBQUUsSUFBQyxDQUFBLEVBQUgsQ0FBTSxDQUFDLFdBQVAsQ0FBbUIsTUFBbkI7SUFETzs7NEJBR1gsSUFBQSxHQUFNLFNBQUE7QUFDRixVQUFBO01BQUEsR0FBQSxHQUFNLENBQUEsQ0FBRSxJQUFDLENBQUEsRUFBSDtNQUNOLEdBQUEsR0FBTSxHQUFHLENBQUMsSUFBSixDQUFTLFVBQVQ7TUFDTixLQUFBLEdBQVEsR0FBRyxDQUFDLElBQUosQ0FBUyxPQUFUO2FBRVIsQ0FBQSxDQUFFLEdBQUYsQ0FBTSxDQUFDLE9BQVAsQ0FBZSxpQkFBZixFQUFrQyxRQUFBLENBQVMsS0FBQSxHQUFRLENBQWpCLENBQWxDO0lBTEU7OzRCQU9OLFVBQUEsR0FBWSxTQUFBO0FBQ1IsVUFBQTtNQUFBLEdBQUEsR0FBTSxDQUFBLENBQUUsSUFBQyxDQUFBLEVBQUg7TUFDTixLQUFBLEdBQVEsR0FBRyxDQUFDLElBQUosQ0FBUyx1QkFBVDtNQUNSLEtBQUEsR0FBUSxHQUFHLENBQUMsSUFBSixDQUFTLE9BQVQ7YUFDUixDQUFBLENBQUUsS0FBRixDQUNJLENBQUMsSUFETCxDQUNVLFVBRFYsQ0FFSSxDQUFDLE9BRkwsQ0FFYSxpQkFGYixFQUVnQyxLQUFBLEdBQVEsQ0FGeEMsQ0FHSSxDQUFDLE9BSEwsQ0FHYSxpQkFIYixFQUdnQyxLQUFBLEdBQVEsQ0FIeEMsQ0FJSSxDQUFDLEVBSkwsQ0FJUSx5QkFKUixFQUltQyxTQUFBO1FBQzNCLENBQUEsQ0FBRSxNQUFGLENBQVMsQ0FBQyxNQUFWLENBQUE7ZUFDQSxPQUFPLENBQUMsR0FBUixDQUFZLFdBQVo7TUFGMkIsQ0FKbkM7SUFKUTs7Ozs7RUFlaEIsb0JBQUEsR0FBdUIsU0FBQyxNQUFEO1dBQ25CLElBQUMsQ0FBQyxJQUFGLENBQU8sU0FBQTtBQUNILFVBQUE7TUFBQSxLQUFBLEdBQVEsQ0FBQSxDQUFFLElBQUY7TUFDUixJQUFBLEdBQU8sS0FBSyxDQUFDLElBQU4sQ0FBVyxtQkFBWDtNQUVQLElBQUcsQ0FBQyxJQUFKO1FBQ0ksS0FBSyxDQUFDLElBQU4sQ0FBVyxtQkFBWCxFQUFnQyxDQUFDLElBQUEsR0FBVyxJQUFBLGNBQUEsQ0FBZSxJQUFmLENBQVosQ0FBaEMsRUFESjs7TUFFQSxJQUFJLE9BQU8sTUFBUCxLQUFpQixRQUFyQjtRQUNJLElBQUssQ0FBQSxNQUFBLENBQUwsQ0FBQSxFQURKOztJQU5HLENBQVA7RUFEbUI7RUFXdkIsbUJBQUEsR0FBc0IsU0FBQyxNQUFELEVBQVMsS0FBVDtXQUNsQixJQUFDLENBQUMsSUFBRixDQUFPLFNBQUE7QUFDSCxVQUFBO01BQUEsS0FBQSxHQUFRLENBQUEsQ0FBRSxJQUFGO01BQ1IsSUFBQSxHQUFPLEtBQUssQ0FBQyxJQUFOLENBQVcsa0JBQVg7TUFFUCxJQUFHLENBQUMsSUFBSjtRQUNJLEtBQUssQ0FBQyxJQUFOLENBQVcsa0JBQVgsRUFBK0IsQ0FBQyxJQUFBLEdBQVcsSUFBQSxhQUFBLENBQWMsSUFBZCxDQUFaLENBQS9CLEVBREo7O01BRUEsSUFBSSxPQUFPLE1BQVAsS0FBaUIsUUFBckI7UUFDSSxJQUFLLENBQUEsTUFBQSxDQUFMLENBQUEsRUFESjs7SUFORyxDQUFQO0VBRGtCO0VBYXRCLENBQUEsQ0FBRSxTQUFBO0FBQ0UsUUFBQTtJQUFBLGNBQUEsR0FBaUIsU0FBQyxDQUFELEVBQUksRUFBSixFQUFRLEtBQVI7TUFDYixJQUFHLENBQUEsR0FBSSxFQUFQO1FBQ0ksb0JBQW9CLENBQUMsSUFBckIsQ0FBMEIsS0FBMUIsRUFBaUMsT0FBakMsRUFESjtPQUFBLE1BQUE7UUFHSSxvQkFBb0IsQ0FBQyxJQUFyQixDQUEwQixLQUExQixFQUFpQyxTQUFqQyxFQUhKOztJQURhO0lBT2pCLGVBQUEsR0FBa0IsU0FBQyxDQUFELEVBQUksRUFBSixFQUFRLEtBQVI7TUFDZCxJQUFHLENBQUEsR0FBSSxFQUFQO1FBQ0ksb0JBQW9CLENBQUMsSUFBckIsQ0FBMEIsS0FBMUIsRUFBaUMsT0FBakMsRUFESjtPQUFBLE1BQUE7UUFHSSxvQkFBb0IsQ0FBQyxJQUFyQixDQUEwQixLQUExQixFQUFpQyxTQUFqQyxFQUhKOztJQURjO0lBT2xCLENBQUEsQ0FBRSxXQUFGLENBQWMsQ0FBQyxJQUFmLENBQW9CLFNBQUE7QUFFaEIsVUFBQTtNQUFBLEtBQUEsR0FBUSxDQUFBLENBQUUsSUFBRjtNQUNSLEVBQUEsR0FBSyxLQUFLLENBQUMsSUFBTixDQUFXLFNBQVg7TUFFTCxJQUFHLEVBQUg7UUFDSSxlQUFBLENBQWdCLENBQUEsQ0FBRSxNQUFGLENBQVMsQ0FBQyxLQUFWLENBQUEsQ0FBaEIsRUFBbUMsRUFBbkMsRUFBdUMsS0FBdkM7UUFDQSxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsRUFBVixDQUFhLFFBQWIsRUFBdUIsU0FBQTtpQkFDbkIsZUFBQSxDQUFnQixDQUFBLENBQUUsSUFBRixDQUFJLENBQUMsS0FBTCxDQUFBLENBQWhCLEVBQThCLEVBQTlCLEVBQWtDLEtBQWxDO1FBRG1CLENBQXZCLEVBRko7T0FBQSxNQUFBO1FBS0ksb0JBQW9CLENBQUMsSUFBckIsQ0FBMEIsS0FBMUIsRUFBaUMsT0FBakMsRUFMSjs7SUFMZ0IsQ0FBcEI7SUFnQkEsQ0FBQSxDQUFFLG1CQUFGLENBQXNCLENBQUMsSUFBdkIsQ0FBNEIsU0FBQTtBQUN4QixVQUFBO01BQUEsS0FBQSxHQUFRLENBQUEsQ0FBRSxJQUFGO01BQ1IsRUFBQSxHQUFLLEtBQUssQ0FBQyxJQUFOLENBQVcsV0FBWDtNQUVMLGNBQUEsQ0FBZSxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsS0FBVixDQUFBLENBQWYsRUFBa0MsRUFBbEMsRUFBc0MsS0FBdEM7YUFFQSxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsRUFBVixDQUFhLFFBQWIsRUFBdUIsU0FBQTtlQUNuQixjQUFBLENBQWUsQ0FBQSxDQUFFLElBQUYsQ0FBSSxDQUFDLEtBQUwsQ0FBQSxDQUFmLEVBQTZCLEVBQTdCLEVBQWlDLEtBQWpDO01BRG1CLENBQXZCO0lBTndCLENBQTVCO0lBV0EsQ0FBQSxDQUFFLGtCQUFGLENBQXFCLENBQUMsRUFBdEIsQ0FBeUIsc0JBQXpCLEVBQWlELFNBQUMsS0FBRDthQUM3QyxPQUFPLENBQUMsR0FBUixDQUFZLEtBQUssQ0FBQyxJQUFsQjtJQUQ2QyxDQUFqRDtJQVdBLFFBQUEsR0FBVyxTQUFDLFFBQUQsRUFBVyxNQUFYLEVBQW1CLEtBQW5CO01BQ1QsU0FBUyxDQUFDLElBQVYsQ0FDRTtRQUFBLE9BQUEsRUFBUyxTQUFUO1FBQ0EsaUJBQUEsRUFBbUIsUUFEbkI7UUFFQSxlQUFBLEVBQWlCLE1BRmpCO1FBR0EsY0FBQSxFQUFnQixLQUhoQjtRQUlBLGNBQUEsRUFBZ0IsTUFKaEI7T0FERjtJQURTO0lBVVgsTUFBQSxHQUFTLFNBQUMsS0FBRCxFQUFRLE1BQVI7TUFDTCxPQUFPLENBQUMsR0FBUixDQUFlLE1BQUQsR0FBUSxHQUFSLEdBQVcsS0FBekI7YUFDQSxRQUFBLENBQVMsU0FBVCxFQUFvQixRQUFwQixFQUFpQyxNQUFELEdBQVEsR0FBUixHQUFXLEtBQTNDO0lBRks7SUFPVCxDQUFBLENBQUUsb0NBQUYsQ0FBdUMsQ0FBQyxFQUF4QyxDQUEyQyx5QkFBM0MsRUFBc0UsU0FBQyxDQUFEO0FBQ2xFLFVBQUE7TUFBQSxNQUFBLEdBQVMsQ0FBQSxDQUFFLElBQUYsQ0FBSSxDQUFDLElBQUwsQ0FBVSxRQUFWO01BQ1QsS0FBQSxHQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBUCxHQUFlO2FBQ3ZCLE1BQUEsQ0FBTyxLQUFQLEVBQWMsTUFBZDtJQUhrRSxDQUF0RTtJQU1BLENBQUEsQ0FBRSxVQUFGLENBQWEsQ0FBQyxFQUFkLENBQWlCLHdCQUFqQixFQUEyQyxTQUFDLENBQUQ7QUFDdkMsVUFBQTtNQUFBLE1BQUEsR0FBUyxDQUFBLENBQUUsSUFBRixDQUFJLENBQUMsSUFBTCxDQUFVLFFBQVY7TUFDVCxLQUFBLEdBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFQLEdBQWU7YUFDdkIsTUFBQSxDQUFPLEtBQVAsRUFBYyxNQUFkO0lBSHVDLENBQTNDO0lBS0EsQ0FBQSxDQUFFLGNBQUYsQ0FBaUIsQ0FBQyxFQUFsQixDQUFxQixVQUFyQixFQUFpQyxTQUFDLENBQUQ7QUFDN0IsVUFBQTtNQUFBLE1BQUEsR0FBUztNQUNULEtBQUEsR0FBUTtNQUNSLEVBQUEsR0FBSyxDQUFBLENBQUUsSUFBRixDQUFJLENBQUMsSUFBTCxDQUFVLFdBQVY7TUFFTCxJQUFHLENBQUEsQ0FBRSxNQUFGLENBQVMsQ0FBQyxLQUFWLENBQUEsQ0FBQSxHQUFvQixFQUF2QjtlQUNJLE1BQUEsQ0FBTyxLQUFQLEVBQWMsTUFBZCxFQURKOztJQUw2QixDQUFqQztJQVdBLE9BQUEsR0FBVSxDQUFBLENBQUUsVUFBRjtJQUNWLE1BQUEsR0FBUyxDQUFBLENBQUUsU0FBRjtJQUdULE9BQU8sQ0FBQyxFQUFSLENBQVcsd0JBQVgsRUFBcUMsU0FBQTtNQUNqQyxPQUFPLENBQUMsR0FBUixDQUFZLE9BQVosRUFBcUIsQ0FBQSxDQUFFLElBQUYsQ0FBckI7YUFDQSxDQUFBLENBQUUsSUFBRixDQUNJLENBQUMsSUFETCxDQUNVLFlBRFYsQ0FFSSxDQUFDLEdBRkwsQ0FHUTtRQUFBLFVBQUEsRUFBWSxRQUFaO09BSFI7SUFGaUMsQ0FBckM7SUFNQSxPQUFPLENBQUMsRUFBUixDQUFXLHlCQUFYLEVBQXNDLFNBQUE7TUFDbEMsT0FBTyxDQUFDLEdBQVIsQ0FBWSxLQUFaLEVBQW1CLENBQUEsQ0FBRSxJQUFGLENBQW5CO2FBQ0EsQ0FBQSxDQUFFLElBQUYsQ0FDSSxDQUFDLElBREwsQ0FDVSxZQURWLENBRUksQ0FBQyxHQUZMLENBR1E7UUFBQSxVQUFBLEVBQVksU0FBWjtPQUhSO0lBRmtDLENBQXRDO0lBU0EsQ0FBQSxDQUFFLGNBQUYsQ0FBaUIsQ0FBQyxJQUFsQixDQUF1QixTQUFBO0FBQ25CLFVBQUE7TUFBQSxDQUFBLEdBQUksQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLE1BQVYsQ0FBQTtNQUNKLE1BQUEsR0FBUztNQUVULENBQUEsQ0FBRSxJQUFGLENBQUksQ0FBQyxHQUFMLENBQVMsWUFBVCxFQUF1QixDQUFBLEdBQUksTUFBM0I7YUFDQSxDQUFBLENBQUUsMkJBQUYsQ0FBOEIsQ0FBQyxHQUEvQixDQUFtQyxZQUFuQyxFQUFpRCxDQUFBLEdBQUksTUFBckQsQ0FBNEQsQ0FBQyxHQUE3RCxDQUFpRSxRQUFqRSxFQUEyRSxDQUFBLENBQUUsVUFBRixDQUFhLENBQUMsTUFBZCxDQUFBLENBQTNFO0lBTG1CLENBQXZCO0lBT0EsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLEVBQVYsQ0FBYSxRQUFiLEVBQXVCLFNBQUE7QUFDbkIsVUFBQTtNQUFBLENBQUEsR0FBSSxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsTUFBVixDQUFBO2FBQ0osQ0FBQSxDQUFFLGNBQUYsQ0FBaUIsQ0FBQyxJQUFsQixDQUF1QixTQUFBO0FBQ25CLFlBQUE7UUFBQSxNQUFBLEdBQVM7UUFFVCxDQUFBLENBQUUsSUFBRixDQUFJLENBQUMsR0FBTCxDQUFTLFlBQVQsRUFBdUIsQ0FBQSxHQUFJLE1BQTNCO2VBQ0EsQ0FBQSxDQUFFLDJCQUFGLENBQThCLENBQUMsR0FBL0IsQ0FBbUMsWUFBbkMsRUFBaUQsQ0FBQSxHQUFJLE1BQXJELENBQTRELENBQUMsR0FBN0QsQ0FBaUUsUUFBakUsRUFBMkUsQ0FBQSxDQUFFLFVBQUYsQ0FBYSxDQUFDLE1BQWQsQ0FBQSxDQUEzRTtNQUptQixDQUF2QjtJQUZtQixDQUF2QjtJQVdBLENBQUEsQ0FBRSxRQUFGLENBQVcsQ0FBQyxFQUFaLENBQWUsT0FBZixFQUF3Qiw4QkFBeEIsRUFBd0QsU0FBQTtBQUVwRCxVQUFBO01BQUEsR0FBQSxHQUFNLENBQUEsQ0FBRSxJQUFGLENBQUksQ0FBQyxJQUFMLENBQVUsV0FBVjtNQUVOLElBQUcsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLEtBQVYsQ0FBQSxDQUFBLElBQXFCLEdBQXhCO1FBQ0ksS0FBQSxHQUFRLENBQUEsQ0FBRSxJQUFGO1FBQ1IsTUFBQSxHQUFTLEtBQUssQ0FBQyxJQUFOLENBQVcsdUJBQVg7UUFDVCxRQUFBLEdBQVcsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLElBQVYsQ0FBZSxXQUFmO1FBS1gsUUFBUSxDQUFDLElBQVQsQ0FBYyxTQUFBO1VBRVYsQ0FBQSxDQUFFLElBQUYsQ0FBSSxDQUFDLElBQUwsQ0FBVSxRQUFWLENBQW1CLENBQUMsSUFBcEIsQ0FBeUIsU0FBQTtBQUNyQixnQkFBQTtZQUFBLEdBQUEsR0FBTSxDQUFBLENBQUUsSUFBRixDQUFJLENBQUMsSUFBTCxDQUFVLEtBQVY7bUJBQ04sQ0FBQSxDQUFFLElBQUYsQ0FBSSxDQUFDLElBQUwsQ0FBVSxLQUFWLEVBQWlCLEdBQWpCO1VBRnFCLENBQXpCO1VBS0Esb0JBQW9CLENBQUMsSUFBckIsQ0FBMkIsQ0FBQSxDQUFFLElBQUYsQ0FBM0IsRUFBaUMsU0FBakM7VUFDQSxvQkFBb0IsQ0FBQyxJQUFyQixDQUEyQixDQUFBLENBQUUsSUFBRixDQUEzQixFQUFpQyxPQUFqQztpQkFFQSxDQUFBLENBQUUsSUFBRixDQUFJLENBQUMsT0FBTCxDQUFhLHNCQUFiO1FBVlUsQ0FBZDtRQVlBLG1CQUFtQixDQUFDLElBQXBCLENBQXlCLENBQUEsQ0FBRSxNQUFGLENBQXpCLEVBQW9DLFdBQXBDO2VBRUEsbUJBQW1CLENBQUMsSUFBcEIsQ0FBeUIsS0FBekIsRUFBZ0MsWUFBaEMsRUF0Qko7O0lBSm9ELENBQXhEO0lBNEJBLENBQUEsQ0FBRSxRQUFGLENBQVcsQ0FBQyxFQUFaLENBQWUsT0FBZixFQUF3QixVQUF4QixFQUFvQyxTQUFBO2FBQ2hDLG1CQUFtQixDQUFDLElBQXBCLENBQXlCLENBQUEsQ0FBRSxRQUFGLENBQXpCLEVBQXNDLFdBQXRDO0lBRGdDLENBQXBDO0lBR0EsQ0FBQSxDQUFFLFFBQUYsQ0FBVyxDQUFDLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFdBQXhCLEVBQXFDLFNBQUE7YUFDakMsbUJBQW1CLENBQUMsSUFBcEIsQ0FBeUIsQ0FBQSxDQUFFLFFBQUYsQ0FBekIsRUFBc0MsV0FBdEM7SUFEaUMsQ0FBckM7SUFHQSxDQUFBLENBQUUsUUFBRixDQUFXLENBQUMsRUFBWixDQUFlLE9BQWYsRUFBd0IsY0FBeEIsRUFBd0MsU0FBQTthQUNwQyxtQkFBbUIsQ0FBQyxJQUFwQixDQUF5QixDQUFBLENBQUUsSUFBRixDQUF6QixFQUErQixNQUEvQjtJQURvQyxDQUF4QztJQUlBLElBQUEsR0FBTztJQUNQLFFBQUEsR0FBVztJQUVYLGNBQUEsR0FBaUIsU0FBQyxDQUFEO01BQ2IsQ0FBQSxDQUFFLGNBQUYsQ0FBaUIsQ0FBQyxXQUFsQixDQUE4QixRQUE5QjthQUNBLENBQUEsQ0FBRSxlQUFBLEdBQWdCLENBQWhCLEdBQWtCLElBQXBCLENBQXlCLENBQUMsUUFBMUIsQ0FBbUMsUUFBbkM7SUFGYTtJQUlqQixPQUFPLENBQUMsRUFBUixDQUFXLHNCQUFYLEVBQW1DLFNBQUMsQ0FBRDtNQUMvQixJQUFHLENBQUMsSUFBSjtRQUNJLElBQUEsR0FBTztRQUNQLE1BQU0sQ0FBQyxPQUFQLENBQWUsaUJBQWYsRUFBa0MsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQVIsRUFBZSxRQUFmLEVBQXlCLElBQXpCLENBQWxDO1FBQ0EsY0FBQSxDQUFlLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBUCxHQUFlLENBQTlCO2VBQ0EsSUFBQSxHQUFPLE1BSlg7O0lBRCtCLENBQW5DO1dBUUEsQ0FBQSxDQUFFLGtCQUFGLENBQXFCLENBQUMsRUFBdEIsQ0FBeUIsc0JBQXpCLEVBQWlELFNBQUMsQ0FBRDthQUM3QyxDQUFBLENBQUUsVUFBRixDQUFhLENBQUMsSUFBZCxDQUFxQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBUCxHQUFlLENBQWhCLENBQUEsR0FBa0IsTUFBbEIsR0FBd0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFwRDtJQUQ2QyxDQUFqRDtFQXRMRixDQUFGO0FBdkhDLENBQUEsQ0FBSCxDQUFRLE1BQVIsRUFBZ0IsTUFBaEIiLCJmaWxlIjoiY2Fyb3VzZWwuY2xhc3MuanMifQ==

$(function() {
  var md;
  md = new MobileDetect(window.navigator.userAgent);
  if (md.os() === 'AndroidOS') {
    $('body').addClass('AndroidOS notiOS');
  } else if (md.os() === 'iOS') {
    $('body').addClass('iOS notAndroidOS');
  }
  if (navigator.userAgent.match(/Windows Phone OS|Windows CE|Windows Mobile|IEMobile|Windows Phone OS 7|Windows Phone 8/i)) {
    $('body').addClass('winphones');
  }
  if (md.mobile()) {
    return $('body').addClass('mobile');
  }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRldGVjdHBsYXRmb3JtLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxDQUFBLENBQUUsU0FBQTtBQUNELE1BQUE7RUFBQSxFQUFBLEdBQVMsSUFBQSxZQUFBLENBQWEsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUE5QjtFQUVULElBQUcsRUFBRSxDQUFDLEVBQUgsQ0FBQSxDQUFBLEtBQVcsV0FBZDtJQUNDLENBQUEsQ0FBRSxNQUFGLENBQVMsQ0FBQyxRQUFWLENBQW1CLGtCQUFuQixFQUREO0dBQUEsTUFFSyxJQUFHLEVBQUUsQ0FBQyxFQUFILENBQUEsQ0FBQSxLQUFXLEtBQWQ7SUFDSixDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsUUFBVixDQUFtQixrQkFBbkIsRUFESTs7RUFHTCxJQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUMsS0FBcEIsQ0FBMEIseUZBQTFCLENBQUg7SUFDQyxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsUUFBVixDQUFtQixXQUFuQixFQUREOztFQUdBLElBQUcsRUFBRSxDQUFDLE1BQUgsQ0FBQSxDQUFIO1dBQ0MsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLFFBQVYsQ0FBbUIsUUFBbkIsRUFERDs7QUFYQyxDQUFGIiwiZmlsZSI6ImRldGVjdHBsYXRmb3JtLmpzIn0=

var setHeightCalc;

setHeightCalc = function(el, r) {
  return $(el).each(function() {
    var $this, c, etalon, set;
    $this = $(this);
    c = $(this).data('c');
    etalon = $(this).find('.etalon');
    $this.addClass('ready');
    set = function() {
      return setTimeout(((function(_this) {
        return function() {
          var h;
          h = Math.floor(etalon.outerHeight(true) * c) - 2;
          $this.data('etalon', h);
          return $this.css({
            height: h
          });
        };
      })(this)), 100);
    };
    if (r === 'big') {
      if ($(window).width() > 490) {
        return set();
      } else {
        return $this.removeAttr('style');
      }
    } else if (r === 'small') {
      if ($(window).width() < 490) {
        return set();
      } else {
        return $this.removeAttr('style');
      }
    } else if (r === 'always') {
      return set();
    }
  });
};

$(window).load(function() {
  var isImageOk;
  isImageOk = function(img) {
    if (!img.complete) {
      return false;
    }
    if (typeof img.naturalWidth !== "undefined" && img.naturalWidth === 0) {
      return false;
    }
    setHeightCalc('.js-max-height', 'always');
    setHeightCalc('.js-max-height-calculation', 'big');
    return true;
  };
  return $('.js-max-height .etalon img, .js-max-height-calculation .etalon img').each(function() {
    return isImageOk(this);
  });
});

$(function() {
  var events, setSrc;
  events = {
    evClick: function(el) {
      var hTr, parent, target, to;
      target = $(el).data('expand');
      to = $(el).data('to');
      if ($(target).data('etalon') > 0) {
        hTr = $(target).data('etalon');
      } else {
        hTr = 0;
      }
      $(el).addClass('hide').siblings().removeClass('hide');
      $(target).toggleClass('open');
      if (to === 'down') {
        return $(target).stop(true).animate({
          height: $(target).get(0).scrollHeight
        }, 500);
      } else if (to === 'up') {
        parent = $(target).parents('.section').attr('id');
        $(target).stop(true).animate({
          height: hTr
        }, 500);
        return $('html,body').scrollTo('#' + parent, '#' + parent);
      }
    }
  };
  setSrc = function() {
    return $('.frames__item img').each(function() {
      var src;
      src = $(this).data('src');
      return $(this).attr('src', src);
    });
  };
  $(document).on('click', '[data-expand]', function() {
    setSrc();
    events.evClick($(this));
  }).on('touchstart', '[data-expand]', function(e) {
    var $this, oldScrollTop;
    oldScrollTop = $(window).scrollTop();
    $this = $(this);
    return setTimeout(((function(_this) {
      return function() {
        var newScrollTop;
        newScrollTop = $(window).scrollTop();
        if (Math.abs(oldScrollTop - newScrollTop) < 3) {
          return events.evClick($this);
        }
      };
    })(this)), 100);
  });
  return $(window).on('resize', function() {
    setHeightCalc('.js-max-height', 'always');
    setHeightCalc('.js-max-height-calculation', 'big');
    setHeightCalc('.handmade-items', 'small');
    $('[data-to="down"]').removeClass('hide');
    return $('[data-to="up"]').addClass('hide');
  });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImV4cGFuZGVycy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsSUFBQTs7QUFBQSxhQUFBLEdBQWdCLFNBQUMsRUFBRCxFQUFLLENBQUw7U0FDWixDQUFBLENBQUUsRUFBRixDQUFLLENBQUMsSUFBTixDQUFXLFNBQUE7QUFDUCxRQUFBO0lBQUEsS0FBQSxHQUFRLENBQUEsQ0FBRSxJQUFGO0lBQ1IsQ0FBQSxHQUFJLENBQUEsQ0FBRSxJQUFGLENBQUksQ0FBQyxJQUFMLENBQVUsR0FBVjtJQUNKLE1BQUEsR0FBUyxDQUFBLENBQUUsSUFBRixDQUFJLENBQUMsSUFBTCxDQUFVLFNBQVY7SUFHVCxLQUFLLENBQUMsUUFBTixDQUFlLE9BQWY7SUFFQSxHQUFBLEdBQU0sU0FBQTthQUNGLFVBQUEsQ0FBVyxDQUFFLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtBQUNULGNBQUE7VUFBQSxDQUFBLEdBQUksSUFBSSxDQUFDLEtBQUwsQ0FBVyxNQUFNLENBQUMsV0FBUCxDQUFtQixJQUFuQixDQUFBLEdBQTJCLENBQXRDLENBQUEsR0FBMkM7VUFDL0MsS0FBSyxDQUFDLElBQU4sQ0FBVyxRQUFYLEVBQXFCLENBQXJCO2lCQUNBLEtBQUssQ0FBQyxHQUFOLENBQ0k7WUFBQSxNQUFBLEVBQVEsQ0FBUjtXQURKO1FBSFM7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQUYsQ0FBWCxFQUtHLEdBTEg7SUFERTtJQVFOLElBQUcsQ0FBQSxLQUFLLEtBQVI7TUFDSSxJQUFHLENBQUEsQ0FBRSxNQUFGLENBQVMsQ0FBQyxLQUFWLENBQUEsQ0FBQSxHQUFvQixHQUF2QjtlQUNJLEdBQUEsQ0FBQSxFQURKO09BQUEsTUFBQTtlQUdJLEtBQUssQ0FBQyxVQUFOLENBQWlCLE9BQWpCLEVBSEo7T0FESjtLQUFBLE1BTUssSUFBRyxDQUFBLEtBQUssT0FBUjtNQUNELElBQUcsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLEtBQVYsQ0FBQSxDQUFBLEdBQW9CLEdBQXZCO2VBQ0ksR0FBQSxDQUFBLEVBREo7T0FBQSxNQUFBO2VBR0ksS0FBSyxDQUFDLFVBQU4sQ0FBaUIsT0FBakIsRUFISjtPQURDO0tBQUEsTUFLQSxJQUFHLENBQUEsS0FBSyxRQUFSO2FBQ0QsR0FBQSxDQUFBLEVBREM7O0VBM0JFLENBQVg7QUFEWTs7QUFnQ2hCLENBQUEsQ0FBRSxNQUFGLENBQVMsQ0FBQyxJQUFWLENBQWUsU0FBQTtBQUNYLE1BQUE7RUFBQSxTQUFBLEdBQVksU0FBQyxHQUFEO0lBRVIsSUFBRyxDQUFDLEdBQUcsQ0FBQyxRQUFSO0FBQ0ksYUFBTyxNQURYOztJQUdBLElBQUcsT0FBTyxHQUFHLENBQUMsWUFBWCxLQUEyQixXQUEzQixJQUEwQyxHQUFHLENBQUMsWUFBSixLQUFvQixDQUFqRTtBQUNJLGFBQU8sTUFEWDs7SUFHQSxhQUFBLENBQWMsZ0JBQWQsRUFBZ0MsUUFBaEM7SUFDQSxhQUFBLENBQWMsNEJBQWQsRUFBNEMsS0FBNUM7QUFDQSxXQUFPO0VBVkM7U0FjWixDQUFBLENBQUUsb0VBQUYsQ0FBdUUsQ0FBQyxJQUF4RSxDQUE2RSxTQUFBO1dBQ3pFLFNBQUEsQ0FBVSxJQUFWO0VBRHlFLENBQTdFO0FBZlcsQ0FBZjs7QUFrQkEsQ0FBQSxDQUFFLFNBQUE7QUFDRSxNQUFBO0VBQUEsTUFBQSxHQUFTO0lBQ0wsT0FBQSxFQUFTLFNBQUMsRUFBRDtBQUNMLFVBQUE7TUFBQSxNQUFBLEdBQVMsQ0FBQSxDQUFFLEVBQUYsQ0FBSyxDQUFDLElBQU4sQ0FBVyxRQUFYO01BQ1QsRUFBQSxHQUFLLENBQUEsQ0FBRSxFQUFGLENBQUssQ0FBQyxJQUFOLENBQVcsSUFBWDtNQUVMLElBQUcsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLElBQVYsQ0FBZSxRQUFmLENBQUEsR0FBMkIsQ0FBOUI7UUFDSSxHQUFBLEdBQU0sQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLElBQVYsQ0FBZSxRQUFmLEVBRFY7T0FBQSxNQUFBO1FBR0ksR0FBQSxHQUFNLEVBSFY7O01BS0EsQ0FBQSxDQUFFLEVBQUYsQ0FDSSxDQUFDLFFBREwsQ0FDYyxNQURkLENBRUksQ0FBQyxRQUZMLENBQUEsQ0FHSSxDQUFDLFdBSEwsQ0FHaUIsTUFIakI7TUFLQSxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsV0FBVixDQUFzQixNQUF0QjtNQUVBLElBQUcsRUFBQSxLQUFNLE1BQVQ7ZUFDSSxDQUFBLENBQUUsTUFBRixDQUNJLENBQUMsSUFETCxDQUNVLElBRFYsQ0FFSSxDQUFDLE9BRkwsQ0FHUTtVQUFBLE1BQUEsRUFBUSxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsR0FBVixDQUFjLENBQWQsQ0FBZ0IsQ0FBQyxZQUF6QjtTQUhSLEVBSVEsR0FKUixFQURKO09BQUEsTUFNSyxJQUFHLEVBQUEsS0FBTSxJQUFUO1FBQ0QsTUFBQSxHQUFTLENBQUEsQ0FBRSxNQUFGLENBQVMsQ0FBQyxPQUFWLENBQWtCLFVBQWxCLENBQTZCLENBQUMsSUFBOUIsQ0FBbUMsSUFBbkM7UUFDVCxDQUFBLENBQUUsTUFBRixDQUNJLENBQUMsSUFETCxDQUNVLElBRFYsQ0FFSSxDQUFDLE9BRkwsQ0FHUTtVQUFBLE1BQUEsRUFBUSxHQUFSO1NBSFIsRUFJUSxHQUpSO2VBS0EsQ0FBQSxDQUFFLFdBQUYsQ0FBYyxDQUFDLFFBQWYsQ0FBd0IsR0FBQSxHQUFJLE1BQTVCLEVBQW9DLEdBQUEsR0FBSSxNQUF4QyxFQVBDOztJQXRCQSxDQURKOztFQWlDVCxNQUFBLEdBQVMsU0FBQTtXQUNMLENBQUEsQ0FBRSxtQkFBRixDQUFzQixDQUFDLElBQXZCLENBQTRCLFNBQUE7QUFDeEIsVUFBQTtNQUFBLEdBQUEsR0FBTSxDQUFBLENBQUUsSUFBRixDQUFJLENBQUMsSUFBTCxDQUFVLEtBQVY7YUFDTixDQUFBLENBQUUsSUFBRixDQUFJLENBQUMsSUFBTCxDQUFVLEtBQVYsRUFBaUIsR0FBakI7SUFGd0IsQ0FBNUI7RUFESztFQU1ULENBQUEsQ0FBRSxRQUFGLENBQ0ksQ0FBQyxFQURMLENBQ1EsT0FEUixFQUNpQixlQURqQixFQUNrQyxTQUFBO0lBRTFCLE1BQUEsQ0FBQTtJQUNBLE1BQU0sQ0FBQyxPQUFQLENBQWUsQ0FBQSxDQUFFLElBQUYsQ0FBZjtFQUgwQixDQURsQyxDQU1JLENBQUMsRUFOTCxDQU1RLFlBTlIsRUFNc0IsZUFOdEIsRUFNdUMsU0FBQyxDQUFEO0FBQy9CLFFBQUE7SUFBQSxZQUFBLEdBQWUsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLFNBQVYsQ0FBQTtJQUNmLEtBQUEsR0FBUSxDQUFBLENBQUUsSUFBRjtXQUNSLFVBQUEsQ0FBVyxDQUFFLENBQUEsU0FBQSxLQUFBO2FBQUEsU0FBQTtBQUNULFlBQUE7UUFBQSxZQUFBLEdBQWUsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLFNBQVYsQ0FBQTtRQUNmLElBQUcsSUFBSSxDQUFDLEdBQUwsQ0FBUyxZQUFBLEdBQWEsWUFBdEIsQ0FBQSxHQUFzQyxDQUF6QztpQkFDSSxNQUFNLENBQUMsT0FBUCxDQUFlLEtBQWYsRUFESjs7TUFGUztJQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBRixDQUFYLEVBSUcsR0FKSDtFQUgrQixDQU52QztTQXNCQSxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsRUFBVixDQUFhLFFBQWIsRUFBdUIsU0FBQTtJQUNuQixhQUFBLENBQWMsZ0JBQWQsRUFBZ0MsUUFBaEM7SUFDQSxhQUFBLENBQWMsNEJBQWQsRUFBNEMsS0FBNUM7SUFDQSxhQUFBLENBQWMsaUJBQWQsRUFBaUMsT0FBakM7SUFDQSxDQUFBLENBQUUsa0JBQUYsQ0FBcUIsQ0FBQyxXQUF0QixDQUFrQyxNQUFsQztXQUNBLENBQUEsQ0FBRSxnQkFBRixDQUFtQixDQUFDLFFBQXBCLENBQTZCLE1BQTdCO0VBTG1CLENBQXZCO0FBOURGLENBQUYiLCJmaWxlIjoiZXhwYW5kZXJzLmpzIn0=

$(function() {});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluaXRwYXJ0aWNsZXNqcy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsQ0FBQSxDQUFFLFNBQUEsR0FBQSxDQUFGIiwiZmlsZSI6ImluaXRwYXJ0aWNsZXNqcy5qcyJ9



//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsb2FkWW91dHViZVZpZGVvcy5qcyJ9

$(function() {
  var delay, framesArray, hash, heroesArray, isInArray, keyControl;
  keyControl = function(el) {
    return $(document).on('keyup', function(e) {
      var type;
      if (e.which === 39) {
        type = 'next';
      } else if (e.which === 37) {
        type = 'prev';
      }
      return el.trigger(type + '.owl.carousel');
    });
  };
  keyControl($('.keycontrol'));
  $(document).on('click', '.iOS .js-click', function() {
    var href;
    href = $(this).attr('href');
    return window.location = href;
  });
  delay = function(ms, func) {
    return setTimeout(func, ms);
  };
  isInArray = function(value, array) {
    return array.indexOf(value) > -1;
  };
  heroesArray = [];
  framesArray = [];
  hash = document.location.hash;
  $('[data-carousel-popup-target="#frames-popup"]').each(function() {
    return framesArray.push('frame-' + ($(this).data('index')));
  });
  $('.carousel-heroes .item').each(function() {
    return heroesArray.push($(this).data('hash'));
  });
  if (isInArray(hash.substring(1), heroesArray)) {
    delay(4000, function() {
      return $('html,body').scrollTo('#heroes', '#heroes', {
        offset: -40
      });
    });
  }
  if (isInArray(hash.substring(1), framesArray)) {
    return delay(3000, function() {
      $('html,body').scrollTo('#frames', '#frames', {
        offset: -40
      });
      return $('[data-carousel-popup-target]').eq(hash.substring(7) - 1).trigger('click');
    });
  }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLENBQUEsQ0FBRSxTQUFBO0FBRUUsTUFBQTtFQUFBLFVBQUEsR0FBYSxTQUFDLEVBQUQ7V0FDVCxDQUFBLENBQUUsUUFBRixDQUFXLENBQUMsRUFBWixDQUFlLE9BQWYsRUFBd0IsU0FBQyxDQUFEO0FBQ3BCLFVBQUE7TUFBQSxJQUFHLENBQUMsQ0FBQyxLQUFGLEtBQVcsRUFBZDtRQUNJLElBQUEsR0FBTyxPQURYO09BQUEsTUFFSyxJQUFHLENBQUMsQ0FBQyxLQUFGLEtBQVcsRUFBZDtRQUNELElBQUEsR0FBTyxPQUROOzthQUVMLEVBQUUsQ0FBQyxPQUFILENBQVcsSUFBQSxHQUFLLGVBQWhCO0lBTG9CLENBQXhCO0VBRFM7RUFRYixVQUFBLENBQVcsQ0FBQSxDQUFFLGFBQUYsQ0FBWDtFQUlBLENBQUEsQ0FBRSxRQUFGLENBQVcsQ0FBQyxFQUFaLENBQWUsT0FBZixFQUF3QixnQkFBeEIsRUFBMEMsU0FBQTtBQUN0QyxRQUFBO0lBQUEsSUFBQSxHQUFPLENBQUEsQ0FBRSxJQUFGLENBQUksQ0FBQyxJQUFMLENBQVUsTUFBVjtXQUNQLE1BQU0sQ0FBQyxRQUFQLEdBQWtCO0VBRm9CLENBQTFDO0VBS0EsS0FBQSxHQUFRLFNBQUMsRUFBRCxFQUFLLElBQUw7V0FBYyxVQUFBLENBQVcsSUFBWCxFQUFpQixFQUFqQjtFQUFkO0VBRVIsU0FBQSxHQUFZLFNBQUMsS0FBRCxFQUFRLEtBQVI7V0FDVixLQUFLLENBQUMsT0FBTixDQUFjLEtBQWQsQ0FBQSxHQUF1QixDQUFDO0VBRGQ7RUFJWixXQUFBLEdBQWM7RUFDZCxXQUFBLEdBQWM7RUFDZCxJQUFBLEdBQU8sUUFBUSxDQUFDLFFBQVEsQ0FBQztFQUV6QixDQUFBLENBQUUsOENBQUYsQ0FBaUQsQ0FBQyxJQUFsRCxDQUF1RCxTQUFBO1dBQ25ELFdBQVcsQ0FBQyxJQUFaLENBQWlCLFFBQUEsR0FBVyxDQUFDLENBQUEsQ0FBRSxJQUFGLENBQUksQ0FBQyxJQUFMLENBQVUsT0FBVixDQUFELENBQTVCO0VBRG1ELENBQXZEO0VBR0EsQ0FBQSxDQUFFLHdCQUFGLENBQTJCLENBQUMsSUFBNUIsQ0FBaUMsU0FBQTtXQUM3QixXQUFXLENBQUMsSUFBWixDQUFpQixDQUFBLENBQUUsSUFBRixDQUFJLENBQUMsSUFBTCxDQUFVLE1BQVYsQ0FBakI7RUFENkIsQ0FBakM7RUFHQSxJQUFHLFNBQUEsQ0FBVSxJQUFJLENBQUMsU0FBTCxDQUFlLENBQWYsQ0FBVixFQUE2QixXQUE3QixDQUFIO0lBQ0ksS0FBQSxDQUFNLElBQU4sRUFBWSxTQUFBO2FBQ1IsQ0FBQSxDQUFFLFdBQUYsQ0FBYyxDQUFDLFFBQWYsQ0FBd0IsU0FBeEIsRUFBbUMsU0FBbkMsRUFBOEM7UUFBQSxNQUFBLEVBQVEsQ0FBQyxFQUFUO09BQTlDO0lBRFEsQ0FBWixFQURKOztFQUlBLElBQUcsU0FBQSxDQUFVLElBQUksQ0FBQyxTQUFMLENBQWUsQ0FBZixDQUFWLEVBQTZCLFdBQTdCLENBQUg7V0FDSSxLQUFBLENBQU0sSUFBTixFQUFZLFNBQUE7TUFDUixDQUFBLENBQUUsV0FBRixDQUFjLENBQUMsUUFBZixDQUF3QixTQUF4QixFQUFtQyxTQUFuQyxFQUE4QztRQUFBLE1BQUEsRUFBUSxDQUFDLEVBQVQ7T0FBOUM7YUFDQSxDQUFBLENBQUUsOEJBQUYsQ0FBaUMsQ0FBQyxFQUFsQyxDQUFxQyxJQUFJLENBQUMsU0FBTCxDQUFlLENBQWYsQ0FBQSxHQUFrQixDQUF2RCxDQUF5RCxDQUFDLE9BQTFELENBQWtFLE9BQWxFO0lBRlEsQ0FBWixFQURKOztBQXZDRixDQUFGIiwiZmlsZSI6Im1haW4uanMifQ==

$(function() {
  var aArray, aChild, aChildren, ahref, i;
  $('#nav a').click(function(e) {
    e.preventDefault();
    $('html,body').scrollTo(this.hash, this.hash, {
      offset: -40
    });
    return history.pushState(null, null, this.hash);
  });
  aChildren = $('#nav li').children();
  aArray = [];
  i = 0;
  while (i < aChildren.length) {
    aChild = aChildren[i];
    ahref = $(aChild).attr('href');
    aArray.push(ahref);
    i++;
  }
  return $(window).scroll(function() {
    var divHeight, divPos, docHeight, navActiveCurrent, theID, windowHeight, windowPos;
    windowPos = $(window).scrollTop() + 40;
    windowHeight = $(window).height();
    docHeight = $(document).height();
    if ($(window).scrollTop() > 0) {
      $('.header').addClass('scrolled');
    } else {
      $('.header').removeClass('scrolled');
    }
    i = 0;
    while (i < aArray.length) {
      theID = aArray[i];
      divPos = $(theID).offset().top || null;
      if (divPos) {
        divHeight = $(theID).height();
        if (windowPos >= divPos && windowPos < divPos + divHeight) {
          $('a[href=\'' + theID + '\']').addClass('nav-active');
        } else {
          $('a[href=\'' + theID + '\']').removeClass('nav-active');
        }
        i++;
      }
    }
    if (windowPos + windowHeight === docHeight) {
      if (!$('#nav li:last-child a').hasClass('nav-active')) {
        navActiveCurrent = $('.nav-active').attr('href');
        $('a[href=\'' + navActiveCurrent + '\']').removeClass('nav-active');
        $('#nav li:last-child a').addClass('nav-active');
      }
    }
  });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5hdmlnYXRpb24uY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLENBQUEsQ0FBRSxTQUFBO0FBQ0QsTUFBQTtFQUFBLENBQUEsQ0FBRSxRQUFGLENBQVcsQ0FBQyxLQUFaLENBQWtCLFNBQUMsQ0FBRDtJQUNoQixDQUFDLENBQUMsY0FBRixDQUFBO0lBQ0EsQ0FBQSxDQUFFLFdBQUYsQ0FBYyxDQUFDLFFBQWYsQ0FBd0IsSUFBQyxDQUFBLElBQXpCLEVBQStCLElBQUMsQ0FBQSxJQUFoQyxFQUFzQztNQUFBLE1BQUEsRUFBUSxDQUFDLEVBQVQ7S0FBdEM7V0FDQSxPQUFPLENBQUMsU0FBUixDQUFrQixJQUFsQixFQUF3QixJQUF4QixFQUE4QixJQUFDLENBQUEsSUFBL0I7RUFIZ0IsQ0FBbEI7RUFNQSxTQUFBLEdBQVksQ0FBQSxDQUFFLFNBQUYsQ0FBWSxDQUFDLFFBQWIsQ0FBQTtFQUVaLE1BQUEsR0FBUztFQUVULENBQUEsR0FBSTtBQUNKLFNBQU0sQ0FBQSxHQUFJLFNBQVMsQ0FBQyxNQUFwQjtJQUNFLE1BQUEsR0FBUyxTQUFVLENBQUEsQ0FBQTtJQUNuQixLQUFBLEdBQVEsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLElBQVYsQ0FBZSxNQUFmO0lBQ1IsTUFBTSxDQUFDLElBQVAsQ0FBWSxLQUFaO0lBQ0EsQ0FBQTtFQUpGO1NBTUEsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLE1BQVYsQ0FBaUIsU0FBQTtBQUNmLFFBQUE7SUFBQSxTQUFBLEdBQVksQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLFNBQVYsQ0FBQSxDQUFBLEdBQXdCO0lBRXBDLFlBQUEsR0FBZSxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsTUFBVixDQUFBO0lBRWYsU0FBQSxHQUFZLENBQUEsQ0FBRSxRQUFGLENBQVcsQ0FBQyxNQUFaLENBQUE7SUFFWixJQUFHLENBQUEsQ0FBRSxNQUFGLENBQVMsQ0FBQyxTQUFWLENBQUEsQ0FBQSxHQUF3QixDQUEzQjtNQUNDLENBQUEsQ0FBRSxTQUFGLENBQVksQ0FBQyxRQUFiLENBQXNCLFVBQXRCLEVBREQ7S0FBQSxNQUFBO01BR0MsQ0FBQSxDQUFFLFNBQUYsQ0FBWSxDQUFDLFdBQWIsQ0FBeUIsVUFBekIsRUFIRDs7SUFNQSxDQUFBLEdBQUk7QUFDSixXQUFNLENBQUEsR0FBSSxNQUFNLENBQUMsTUFBakI7TUFDRSxLQUFBLEdBQVEsTUFBTyxDQUFBLENBQUE7TUFDZixNQUFBLEdBQVMsQ0FBQSxDQUFFLEtBQUYsQ0FBUSxDQUFDLE1BQVQsQ0FBQSxDQUFpQixDQUFDLEdBQWxCLElBQXlCO01BRWxDLElBQUcsTUFBSDtRQUNDLFNBQUEsR0FBWSxDQUFBLENBQUUsS0FBRixDQUFRLENBQUMsTUFBVCxDQUFBO1FBRVosSUFBRyxTQUFBLElBQWEsTUFBYixJQUF3QixTQUFBLEdBQVksTUFBQSxHQUFTLFNBQWhEO1VBQ0UsQ0FBQSxDQUFFLFdBQUEsR0FBYyxLQUFkLEdBQXNCLEtBQXhCLENBQThCLENBQUMsUUFBL0IsQ0FBd0MsWUFBeEMsRUFERjtTQUFBLE1BQUE7VUFHRSxDQUFBLENBQUUsV0FBQSxHQUFjLEtBQWQsR0FBc0IsS0FBeEIsQ0FBOEIsQ0FBQyxXQUEvQixDQUEyQyxZQUEzQyxFQUhGOztRQUlBLENBQUEsR0FQRDs7SUFKRjtJQVlDLElBQUcsU0FBQSxHQUFZLFlBQVosS0FBNEIsU0FBL0I7TUFDRSxJQUFHLENBQUMsQ0FBQSxDQUFFLHNCQUFGLENBQXlCLENBQUMsUUFBMUIsQ0FBbUMsWUFBbkMsQ0FBSjtRQUNFLGdCQUFBLEdBQW1CLENBQUEsQ0FBRSxhQUFGLENBQWdCLENBQUMsSUFBakIsQ0FBc0IsTUFBdEI7UUFDbkIsQ0FBQSxDQUFFLFdBQUEsR0FBYyxnQkFBZCxHQUFpQyxLQUFuQyxDQUF5QyxDQUFDLFdBQTFDLENBQXNELFlBQXREO1FBQ0EsQ0FBQSxDQUFFLHNCQUFGLENBQXlCLENBQUMsUUFBMUIsQ0FBbUMsWUFBbkMsRUFIRjtPQURGOztFQTFCYyxDQUFqQjtBQWxCQyxDQUFGIiwiZmlsZSI6Im5hdmlnYXRpb24uanMifQ==

window.addEventListener('load', (function() {
  var d, desc, e, f, fbQuery, fn, i, itemJ, j, k, l, nameJ, names, path, s, t, u, vkImage;
  e = document.getElementsByTagName('div');
  path = function(name) {
    var b, m, p, sc, scL, sr;
    sc = document.getElementsByTagName('script');
    sr = new RegExp('^(.*/|)(' + name + ')([#?]|$)');
    p = 0;
    scL = sc.length;
    while (p < scL) {
      m = String(sc[p].src).match(sr);
      if (m) {
        if (m[1].match(/^((https?|file)\:\/{2,}|\w:[\/\\])/)) {
          return m[1];
        }
        if (m[1].indexOf('/') === 0) {
          return m[1];
        }
        b = document.getElementsByTagName('base');
        if (b[0] && b[0].href) {
          return b[0].href + m[1];
        } else {
          return document.location.pathname.match(/(.*[\/\\])/)[0] + m[1];
        }
      }
      p++;
    }
    return null;
  };
  desc = function() {
    var m, meta;
    meta = document.getElementsByTagName('meta');
    m = 0;
    while (m < meta.length) {
      if (meta[m].name.toLowerCase() === 'description') {
        return meta[m].content;
      }
      m++;
    }
    return '';
  };
  k = 0;
  while (k < e.length) {
    if (e[k].className.indexOf('share__init') !== -1) {
      if (e[k].getAttribute('data-url') !== -1) {
        u = e[k].getAttribute('data-url');
      }
      if (e[k].getAttribute('data-title') !== -1) {
        t = e[k].getAttribute('data-title');
      }
      if (e[k].getAttribute('data-image') !== -1) {
        i = e[k].getAttribute('data-image');
      }
      if (e[k].getAttribute('data-description') !== -1) {
        d = e[k].getAttribute('data-description');
      }
      if (e[k].getAttribute('data-path') !== -1) {
        f = e[k].getAttribute('data-path');
      }
      if (e[k].getAttribute('data-icons-file') !== -1) {
        fn = e[k].getAttribute('data-icons-file');
      }
      if (!f) {
        f = path('share42.js');
      }
      if (!u) {
        u = location.href;
      }
      if (!t) {
        t = document.title;
      }
      if (!fn) {
        fn = 'icons.png';
      }
      if (!d) {
        d = desc();
      }
      u = encodeURIComponent(u);
      t = encodeURIComponent(t);
      t = t.replace(/\'/g, '%27');
      i = encodeURIComponent(i);
      d = encodeURIComponent(d);
      d = d.replace(/\'/g, '%27');
      fbQuery = 'u=' + u;
      if (i !== 'null' && i !== '') {
        fbQuery = 's=100&p[url]=' + u + '&p[title]=' + t + '&p[summary]=' + d + '&p[images][0]=' + i;
      }
      vkImage = '';
      if (i !== 'null' && i !== '') {
        vkImage = '&image=' + i;
      }
      s = ['data-link-class="facebook"  data-count="fb" onclick="window.open(\'//www.facebook.com/sharer.php?m2w&' + fbQuery + '\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0\');return false" title="Поделиться в Facebook"', 'data-link-class="ok"  data-count="ok" onclick="window.open(\'//ok.ru/dk?st.cmd=addShare&st._surl=' + u + '&title=' + t + '\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0\');return false" title="Добавить в Одноклассники"', 'data-link-class="twitter"  data-count="twi" onclick="window.open(\'//twitter.com/intent/tweet?text=' + t + '&url=' + u + '\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0\');return false" title="Добавить в Twitter"'];
      l = '';
      j = 0;
      names = ['fb', 'ok', 'tw'];
      while (j < s.length) {
        nameJ = names[j];
        itemJ = s[j];
        l += '<span class="socials__link font-icon socials--' + nameJ + '" ' + itemJ + '></span>';
        j++;
      }
      e[k].innerHTML = l;
    }
    k++;
  }
}), false);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNoYXJlLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLENBQUMsZ0JBQVAsQ0FBd0IsTUFBeEIsRUFBZ0MsQ0FBQyxTQUFBO0FBQy9CLE1BQUE7RUFBQSxDQUFBLEdBQUksUUFBUSxDQUFDLG9CQUFULENBQThCLEtBQTlCO0VBRUosSUFBQSxHQUFPLFNBQUMsSUFBRDtBQUNMLFFBQUE7SUFBQSxFQUFBLEdBQUssUUFBUSxDQUFDLG9CQUFULENBQThCLFFBQTlCO0lBQ0wsRUFBQSxHQUFTLElBQUEsTUFBQSxDQUFPLFVBQUEsR0FBYSxJQUFiLEdBQW9CLFdBQTNCO0lBQ1QsQ0FBQSxHQUFJO0lBQ0osR0FBQSxHQUFNLEVBQUUsQ0FBQztBQUNULFdBQU0sQ0FBQSxHQUFJLEdBQVY7TUFDRSxDQUFBLEdBQUksTUFBQSxDQUFPLEVBQUcsQ0FBQSxDQUFBLENBQUUsQ0FBQyxHQUFiLENBQWlCLENBQUMsS0FBbEIsQ0FBd0IsRUFBeEI7TUFDSixJQUFHLENBQUg7UUFDRSxJQUFHLENBQUUsQ0FBQSxDQUFBLENBQUUsQ0FBQyxLQUFMLENBQVcsb0NBQVgsQ0FBSDtBQUNFLGlCQUFPLENBQUUsQ0FBQSxDQUFBLEVBRFg7O1FBRUEsSUFBRyxDQUFFLENBQUEsQ0FBQSxDQUFFLENBQUMsT0FBTCxDQUFhLEdBQWIsQ0FBQSxLQUFxQixDQUF4QjtBQUNFLGlCQUFPLENBQUUsQ0FBQSxDQUFBLEVBRFg7O1FBRUEsQ0FBQSxHQUFJLFFBQVEsQ0FBQyxvQkFBVCxDQUE4QixNQUE5QjtRQUNKLElBQUcsQ0FBRSxDQUFBLENBQUEsQ0FBRixJQUFTLENBQUUsQ0FBQSxDQUFBLENBQUUsQ0FBQyxJQUFqQjtBQUNFLGlCQUFPLENBQUUsQ0FBQSxDQUFBLENBQUUsQ0FBQyxJQUFMLEdBQVksQ0FBRSxDQUFBLENBQUEsRUFEdkI7U0FBQSxNQUFBO0FBR0UsaUJBQU8sUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBM0IsQ0FBaUMsWUFBakMsQ0FBK0MsQ0FBQSxDQUFBLENBQS9DLEdBQW9ELENBQUUsQ0FBQSxDQUFBLEVBSC9EO1NBTkY7O01BVUEsQ0FBQTtJQVpGO1dBYUE7RUFsQks7RUFvQlAsSUFBQSxHQUFPLFNBQUE7QUFDTCxRQUFBO0lBQUEsSUFBQSxHQUFPLFFBQVEsQ0FBQyxvQkFBVCxDQUE4QixNQUE5QjtJQUNQLENBQUEsR0FBSTtBQUNKLFdBQU0sQ0FBQSxHQUFJLElBQUksQ0FBQyxNQUFmO01BQ0UsSUFBRyxJQUFLLENBQUEsQ0FBQSxDQUFFLENBQUMsSUFBSSxDQUFDLFdBQWIsQ0FBQSxDQUFBLEtBQThCLGFBQWpDO0FBQ0UsZUFBTyxJQUFLLENBQUEsQ0FBQSxDQUFFLENBQUMsUUFEakI7O01BRUEsQ0FBQTtJQUhGO1dBSUE7RUFQSztFQVNQLENBQUEsR0FBSTtBQUNKLFNBQU0sQ0FBQSxHQUFJLENBQUMsQ0FBQyxNQUFaO0lBQ0UsSUFBRyxDQUFFLENBQUEsQ0FBQSxDQUFFLENBQUMsU0FBUyxDQUFDLE9BQWYsQ0FBdUIsYUFBdkIsQ0FBQSxLQUF5QyxDQUFDLENBQTdDO01BQ0UsSUFBRyxDQUFFLENBQUEsQ0FBQSxDQUFFLENBQUMsWUFBTCxDQUFrQixVQUFsQixDQUFBLEtBQWlDLENBQUMsQ0FBckM7UUFDRSxDQUFBLEdBQUksQ0FBRSxDQUFBLENBQUEsQ0FBRSxDQUFDLFlBQUwsQ0FBa0IsVUFBbEIsRUFETjs7TUFFQSxJQUFHLENBQUUsQ0FBQSxDQUFBLENBQUUsQ0FBQyxZQUFMLENBQWtCLFlBQWxCLENBQUEsS0FBbUMsQ0FBQyxDQUF2QztRQUNFLENBQUEsR0FBSSxDQUFFLENBQUEsQ0FBQSxDQUFFLENBQUMsWUFBTCxDQUFrQixZQUFsQixFQUROOztNQUVBLElBQUcsQ0FBRSxDQUFBLENBQUEsQ0FBRSxDQUFDLFlBQUwsQ0FBa0IsWUFBbEIsQ0FBQSxLQUFtQyxDQUFDLENBQXZDO1FBQ0UsQ0FBQSxHQUFJLENBQUUsQ0FBQSxDQUFBLENBQUUsQ0FBQyxZQUFMLENBQWtCLFlBQWxCLEVBRE47O01BRUEsSUFBRyxDQUFFLENBQUEsQ0FBQSxDQUFFLENBQUMsWUFBTCxDQUFrQixrQkFBbEIsQ0FBQSxLQUF5QyxDQUFDLENBQTdDO1FBQ0UsQ0FBQSxHQUFJLENBQUUsQ0FBQSxDQUFBLENBQUUsQ0FBQyxZQUFMLENBQWtCLGtCQUFsQixFQUROOztNQUVBLElBQUcsQ0FBRSxDQUFBLENBQUEsQ0FBRSxDQUFDLFlBQUwsQ0FBa0IsV0FBbEIsQ0FBQSxLQUFrQyxDQUFDLENBQXRDO1FBQ0UsQ0FBQSxHQUFJLENBQUUsQ0FBQSxDQUFBLENBQUUsQ0FBQyxZQUFMLENBQWtCLFdBQWxCLEVBRE47O01BRUEsSUFBRyxDQUFFLENBQUEsQ0FBQSxDQUFFLENBQUMsWUFBTCxDQUFrQixpQkFBbEIsQ0FBQSxLQUF3QyxDQUFDLENBQTVDO1FBQ0UsRUFBQSxHQUFLLENBQUUsQ0FBQSxDQUFBLENBQUUsQ0FBQyxZQUFMLENBQWtCLGlCQUFsQixFQURQOztNQUVBLElBQUcsQ0FBQyxDQUFKO1FBQ0UsQ0FBQSxHQUFJLElBQUEsQ0FBSyxZQUFMLEVBRE47O01BRUEsSUFBRyxDQUFDLENBQUo7UUFDRSxDQUFBLEdBQUksUUFBUSxDQUFDLEtBRGY7O01BRUEsSUFBRyxDQUFDLENBQUo7UUFDRSxDQUFBLEdBQUksUUFBUSxDQUFDLE1BRGY7O01BRUEsSUFBRyxDQUFDLEVBQUo7UUFDRSxFQUFBLEdBQUssWUFEUDs7TUFFQSxJQUFHLENBQUMsQ0FBSjtRQUNFLENBQUEsR0FBSSxJQUFBLENBQUEsRUFETjs7TUFFQSxDQUFBLEdBQUksa0JBQUEsQ0FBbUIsQ0FBbkI7TUFDSixDQUFBLEdBQUksa0JBQUEsQ0FBbUIsQ0FBbkI7TUFDSixDQUFBLEdBQUksQ0FBQyxDQUFDLE9BQUYsQ0FBVSxLQUFWLEVBQWlCLEtBQWpCO01BQ0osQ0FBQSxHQUFJLGtCQUFBLENBQW1CLENBQW5CO01BQ0osQ0FBQSxHQUFJLGtCQUFBLENBQW1CLENBQW5CO01BQ0osQ0FBQSxHQUFJLENBQUMsQ0FBQyxPQUFGLENBQVUsS0FBVixFQUFpQixLQUFqQjtNQUNKLE9BQUEsR0FBVSxJQUFBLEdBQU87TUFDakIsSUFBRyxDQUFBLEtBQUssTUFBTCxJQUFnQixDQUFBLEtBQUssRUFBeEI7UUFDRSxPQUFBLEdBQVUsZUFBQSxHQUFrQixDQUFsQixHQUFzQixZQUF0QixHQUFxQyxDQUFyQyxHQUF5QyxjQUF6QyxHQUEwRCxDQUExRCxHQUE4RCxnQkFBOUQsR0FBaUYsRUFEN0Y7O01BRUEsT0FBQSxHQUFVO01BQ1YsSUFBRyxDQUFBLEtBQUssTUFBTCxJQUFnQixDQUFBLEtBQUssRUFBeEI7UUFDRSxPQUFBLEdBQVUsU0FBQSxHQUFZLEVBRHhCOztNQUVBLENBQUEsR0FBSSxDQUNGLHVHQUFBLEdBQTBHLE9BQTFHLEdBQW9ILHNLQURsSCxFQUVGLG1HQUFBLEdBQXNHLENBQXRHLEdBQTBHLFNBQTFHLEdBQXNILENBQXRILEdBQTBILHlLQUZ4SCxFQUdGLHFHQUFBLEdBQXdHLENBQXhHLEdBQTRHLE9BQTVHLEdBQXNILENBQXRILEdBQTBILG1LQUh4SDtNQUtKLENBQUEsR0FBSTtNQUNKLENBQUEsR0FBSTtNQUNKLEtBQUEsR0FBUSxDQUFDLElBQUQsRUFBTyxJQUFQLEVBQWEsSUFBYjtBQUNSLGFBQU0sQ0FBQSxHQUFJLENBQUMsQ0FBQyxNQUFaO1FBQ0UsS0FBQSxHQUFRLEtBQU0sQ0FBQSxDQUFBO1FBQ2QsS0FBQSxHQUFRLENBQUUsQ0FBQSxDQUFBO1FBRVYsQ0FBQSxJQUFLLGdEQUFBLEdBQWlELEtBQWpELEdBQXVELElBQXZELEdBQTRELEtBQTVELEdBQWtFO1FBQ3ZFLENBQUE7TUFMRjtNQU1BLENBQUUsQ0FBQSxDQUFBLENBQUUsQ0FBQyxTQUFMLEdBQWlCLEVBakRuQjs7SUFrREEsQ0FBQTtFQW5ERjtBQWpDK0IsQ0FBRCxDQUFoQyxFQXNGRyxLQXRGSCIsImZpbGUiOiJzaGFyZS5qcyJ9

+(function($, window) {
  'use strict';
  var Plugin, TrailerPopup;
  TrailerPopup = (function() {
    function TrailerPopup(element) {
      this.el = element;
      this.trailerContainer = $('#trailer');
    }

    TrailerPopup.prototype.showPopup = function() {
      return $(this.el).addClass('show');
    };

    TrailerPopup.prototype.hidePopup = function() {
      return $(this.el).removeClass('show');
    };

    TrailerPopup.prototype.setSrc = function() {
      var src;
      src = $(this.el).data('src');
      return $(this.trailerContainer).attr('src', src);
    };

    return TrailerPopup;

  })();
  Plugin = function(option, value) {
    return this.each(function() {
      var $this, data;
      $this = $(this);
      data = $this.data('cl.trailerpopup');
      if (!data) {
        $this.data('cl.trailerpopup', (data = new TrailerPopup(this)));
      }
      if (typeof option === 'string') {
        data[option]();
      }
    });
  };
  return $(function() {
    var events;
    events = {
      evClick: function(el) {
        var $this, target;
        $this = $(el);
        target = $this.data('popup-target');
        Plugin.call($this, 'setSrc');
        return Plugin.call($(target), 'showPopup');
      }
    };
    $(document).on('click', '[data-popup-target]', function(e) {
      var $this;
      e.preventDefault();
      $this = $(this);
      return events.evClick($this);
    }).on('touchstart', '[data-popup-target]', function(e) {
      var $this, oldScrollTop;
      oldScrollTop = $(window).scrollTop();
      $this = $(this);
      return setTimeout(((function(_this) {
        return function() {
          var newScrollTop;
          newScrollTop = $(window).scrollTop();
          if (Math.abs(oldScrollTop - newScrollTop) < 3) {
            return events.evClick($this);
          }
        };
      })(this)), 200);
    });
    $(document).on('click', '.overlay', function() {
      Plugin.call($('.popup'), 'hidePopup');
      return $('#trailer').attr('src', '');
    });
    return $(document).on('click', '.js-close', function() {
      Plugin.call($('.popup'), 'hidePopup');
      return $('#trailer').attr('src', '');
    });
  });
})(jQuery, window);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRyYWlsZXJzLXBvcHVwLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxDQUFLLENBQUEsU0FBQyxDQUFELEVBQWEsTUFBYjtFQUNEO0FBQUEsTUFBQTtFQUNNO0lBQ1csc0JBQUMsT0FBRDtNQUNULElBQUMsQ0FBQSxFQUFELEdBQU07TUFDTixJQUFDLENBQUEsZ0JBQUQsR0FBb0IsQ0FBQSxDQUFFLFVBQUY7SUFGWDs7MkJBSWIsU0FBQSxHQUFXLFNBQUE7YUFDUCxDQUFBLENBQUUsSUFBQyxDQUFBLEVBQUgsQ0FBTSxDQUFDLFFBQVAsQ0FBZ0IsTUFBaEI7SUFETzs7MkJBR1gsU0FBQSxHQUFXLFNBQUE7YUFDUCxDQUFBLENBQUUsSUFBQyxDQUFBLEVBQUgsQ0FBTSxDQUFDLFdBQVAsQ0FBbUIsTUFBbkI7SUFETzs7MkJBR1gsTUFBQSxHQUFRLFNBQUE7QUFDSixVQUFBO01BQUEsR0FBQSxHQUFNLENBQUEsQ0FBRSxJQUFDLENBQUEsRUFBSCxDQUFNLENBQUMsSUFBUCxDQUFZLEtBQVo7YUFDTixDQUFBLENBQUUsSUFBQyxDQUFBLGdCQUFILENBQW9CLENBQUMsSUFBckIsQ0FBMEIsS0FBMUIsRUFBaUMsR0FBakM7SUFGSTs7Ozs7RUFJWixNQUFBLEdBQVMsU0FBQyxNQUFELEVBQVMsS0FBVDtXQUNMLElBQUMsQ0FBQyxJQUFGLENBQU8sU0FBQTtBQUNILFVBQUE7TUFBQSxLQUFBLEdBQVEsQ0FBQSxDQUFFLElBQUY7TUFDUixJQUFBLEdBQU8sS0FBSyxDQUFDLElBQU4sQ0FBVyxpQkFBWDtNQUVQLElBQUcsQ0FBQyxJQUFKO1FBQ0ksS0FBSyxDQUFDLElBQU4sQ0FBVyxpQkFBWCxFQUE4QixDQUFDLElBQUEsR0FBVyxJQUFBLFlBQUEsQ0FBYSxJQUFiLENBQVosQ0FBOUIsRUFESjs7TUFFQSxJQUFJLE9BQU8sTUFBUCxLQUFpQixRQUFyQjtRQUNJLElBQUssQ0FBQSxNQUFBLENBQUwsQ0FBQSxFQURKOztJQU5HLENBQVA7RUFESztTQVdULENBQUEsQ0FBRSxTQUFBO0FBRUUsUUFBQTtJQUFBLE1BQUEsR0FBUztNQUNMLE9BQUEsRUFBUyxTQUFDLEVBQUQ7QUFDTCxZQUFBO1FBQUEsS0FBQSxHQUFRLENBQUEsQ0FBRSxFQUFGO1FBQ1IsTUFBQSxHQUFTLEtBQUssQ0FBQyxJQUFOLENBQVcsY0FBWDtRQUVULE1BQU0sQ0FBQyxJQUFQLENBQVksS0FBWixFQUFtQixRQUFuQjtlQUNBLE1BQU0sQ0FBQyxJQUFQLENBQVksQ0FBQSxDQUFFLE1BQUYsQ0FBWixFQUF1QixXQUF2QjtNQUxLLENBREo7O0lBU1QsQ0FBQSxDQUFFLFFBQUYsQ0FDSSxDQUFDLEVBREwsQ0FDUSxPQURSLEVBQ2lCLHFCQURqQixFQUN3QyxTQUFDLENBQUQ7QUFDaEMsVUFBQTtNQUFBLENBQUMsQ0FBQyxjQUFGLENBQUE7TUFDQSxLQUFBLEdBQVEsQ0FBQSxDQUFFLElBQUY7YUFDUixNQUFNLENBQUMsT0FBUCxDQUFlLEtBQWY7SUFIZ0MsQ0FEeEMsQ0FLSSxDQUFDLEVBTEwsQ0FLUSxZQUxSLEVBS3NCLHFCQUx0QixFQUs2QyxTQUFDLENBQUQ7QUFDckMsVUFBQTtNQUFBLFlBQUEsR0FBZSxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsU0FBVixDQUFBO01BQ2YsS0FBQSxHQUFRLENBQUEsQ0FBRSxJQUFGO2FBQ1IsVUFBQSxDQUFXLENBQUUsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO0FBQ1QsY0FBQTtVQUFBLFlBQUEsR0FBZSxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsU0FBVixDQUFBO1VBQ2YsSUFBRyxJQUFJLENBQUMsR0FBTCxDQUFTLFlBQUEsR0FBYSxZQUF0QixDQUFBLEdBQXNDLENBQXpDO21CQUNJLE1BQU0sQ0FBQyxPQUFQLENBQWUsS0FBZixFQURKOztRQUZTO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFGLENBQVgsRUFJRyxHQUpIO0lBSHFDLENBTDdDO0lBY0EsQ0FBQSxDQUFFLFFBQUYsQ0FBVyxDQUFDLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFVBQXhCLEVBQW9DLFNBQUE7TUFDaEMsTUFBTSxDQUFDLElBQVAsQ0FBWSxDQUFBLENBQUUsUUFBRixDQUFaLEVBQXlCLFdBQXpCO2FBQ0EsQ0FBQSxDQUFFLFVBQUYsQ0FBYSxDQUFDLElBQWQsQ0FBbUIsS0FBbkIsRUFBMEIsRUFBMUI7SUFGZ0MsQ0FBcEM7V0FJQSxDQUFBLENBQUUsUUFBRixDQUFXLENBQUMsRUFBWixDQUFlLE9BQWYsRUFBd0IsV0FBeEIsRUFBcUMsU0FBQTtNQUNqQyxNQUFNLENBQUMsSUFBUCxDQUFZLENBQUEsQ0FBRSxRQUFGLENBQVosRUFBeUIsV0FBekI7YUFDQSxDQUFBLENBQUUsVUFBRixDQUFhLENBQUMsSUFBZCxDQUFtQixLQUFuQixFQUEwQixFQUExQjtJQUZpQyxDQUFyQztFQTdCRixDQUFGO0FBNUJDLENBQUEsQ0FBSCxDQUFRLE1BQVIsRUFBZ0IsTUFBaEIiLCJmaWxlIjoidHJhaWxlcnMtcG9wdXAuanMifQ==

var onYouTubeIframeAPIReady, playYVideo, playerYoutube, stopYVideo;

playerYoutube = void 0;

onYouTubeIframeAPIReady = function() {
  return playerYoutube = new YT.Player('my-video-container', {
    height: '100%',
    width: '100%',
    playerVars: {
      'autoplay': 0,
      'controls': 1,
      'autohide': 1,
      'wmode': 'opaque',
      'showinfo': 0
    },
    events: {
      'onReady': function(e) {}
    },
    videoId: videoId
  });
};

playYVideo = function() {
  return playerYoutube.playVideo();
};

stopYVideo = function() {
  return playerYoutube.stopVideo();
};

$(function() {
  var closeYoutubeContainer, embedVideo, endPoint, events, flagSCroll, globalVideoContainer, playBtn, playWistiaVideo, playYoutubeEmbed, setParams, stopWistiaVideo, stopYoutubeEmbed, turnOff, videoOptions, viewVideos, wistiaContainer, wistiaEmbed, youtubeContainer;
  globalVideoContainer = $('#video_container');
  wistiaContainer = $('.wistia_embed');
  youtubeContainer = $('#y-video');
  closeYoutubeContainer = $('#closeYoutubeVideo');
  playBtn = $('#playBtn');
  viewVideos = $('#view-videos');
  flagSCroll = true;
  events = {};
  endPoint = 800;
  playYoutubeEmbed = function() {
    playYVideo();
    flagSCroll = false;
  };
  stopYoutubeEmbed = function() {
    stopYVideo();
    flagSCroll = true;
  };
  setParams = function(w, h) {
    if (w >= endPoint) {
      globalVideoContainer.css({
        height: h
      });
    } else {
      globalVideoContainer.css({
        height: 100 + '%'
      });
    }
  };
  if (window.preview.state === 'video') {
    wistiaEmbed = void 0;
    videoOptions = {
      plugin: {},
      volume: 0,
      videoFoam: true,
      autoPlay: true
    };
    embedVideo = (function() {
      var init;
      init = function(options, w) {
        if (w <= endPoint) {
          options.plugin = {};
        } else {
          options.plugin = {
            cropFill: {
              src: 'http://fast.wistia.com/labs/crop-fill/plugin.js'
            }
          };
        }
        wistiaEmbed = Wistia.embed(embedId, options);
        wistiaEmbed.bind('play', function() {
          wistiaEmbed.pause();
          wistiaEmbed.time(0);
          wistiaEmbed.play();
          return this.unbind;
        });
        wistiaEmbed.bind('end', function() {
          wistiaEmbed.play();
        });
      };
      init(videoOptions, $(window).width());
      $(window).on('resize', function() {
        Wistia.rebind();
        init(videoOptions, $(window).width());
      });
    })();
    playWistiaVideo = function() {
      wistiaEmbed.play();
    };
    stopWistiaVideo = function() {
      wistiaEmbed.pause();
    };
    turnOff = function() {
      $(window).on('scroll', function() {
        var $this, h, wh;
        $this = $(this);
        if (flagSCroll === true) {
          h = globalVideoContainer.height();
          wh = $this.scrollTop() + $this.height() / 2;
          if (h < wh) {
            stopWistiaVideo();
          } else {
            playWistiaVideo();
          }
        }
      });
    };
    setParams($(window).width(), $(window).height());
    turnOff();
    events = {
      evPlay: function() {
        stopWistiaVideo();
        wistiaContainer.hide();
        youtubeContainer.addClass('show');
        closeYoutubeContainer.addClass('show');
        return playYoutubeEmbed();
      },
      evClose: function(el) {
        $(el).removeClass('show');
        stopYoutubeEmbed();
        wistiaContainer.show();
        youtubeContainer.removeClass('show');
        return playWistiaVideo();
      }
    };
    $(window).on('resize orientationchange', function() {
      return turnOff();
    });
  }
  if (window.preview.state === 'image') {
    events = {
      evPlay: function() {
        youtubeContainer.addClass('show');
        closeYoutubeContainer.addClass('show');
        return playYoutubeEmbed();
      },
      evClose: function(el) {
        $(el).removeClass('show');
        stopYoutubeEmbed();
        return youtubeContainer.removeClass('show');
      }
    };
  }
  $(document).on('click touchstart', '#playBtn', function() {
    return events.evPlay();
  });
  $(document).on('click touchstart', '#closeYoutubeVideo', function() {
    return events.evClose($(this));
  });
  $(window).on('resize orientationchange', function() {
    return setParams($(this).width(), $(this).height());
  });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZpZGVvc2NyZWVuLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxJQUFBOztBQUFBLGFBQUEsR0FBZ0I7O0FBS2hCLHVCQUFBLEdBQTBCLFNBQUE7U0FDeEIsYUFBQSxHQUFvQixJQUFDLEVBQUUsQ0FBQyxNQUFKLENBQVksb0JBQVosRUFDbEI7SUFBQSxNQUFBLEVBQVEsTUFBUjtJQUNBLEtBQUEsRUFBTyxNQURQO0lBRUEsVUFBQSxFQUNHO01BQUEsVUFBQSxFQUFZLENBQVo7TUFDQSxVQUFBLEVBQVksQ0FEWjtNQUVBLFVBQUEsRUFBVyxDQUZYO01BR0EsT0FBQSxFQUFRLFFBSFI7TUFJQSxVQUFBLEVBQVcsQ0FKWDtLQUhIO0lBUUEsTUFBQSxFQUNFO01BQUEsU0FBQSxFQUFXLFNBQUMsQ0FBRCxHQUFBLENBQVg7S0FURjtJQVVBLE9BQUEsRUFBUyxPQVZUO0dBRGtCO0FBREk7O0FBYzFCLFVBQUEsR0FBYSxTQUFBO1NBQ1gsYUFBYSxDQUFDLFNBQWQsQ0FBQTtBQURXOztBQUdiLFVBQUEsR0FBYSxTQUFBO1NBQ1gsYUFBYSxDQUFDLFNBQWQsQ0FBQTtBQURXOztBQUliLENBQUEsQ0FBRSxTQUFBO0FBQ0EsTUFBQTtFQUFBLG9CQUFBLEdBQXVCLENBQUEsQ0FBRSxrQkFBRjtFQUN2QixlQUFBLEdBQWtCLENBQUEsQ0FBRSxlQUFGO0VBQ2xCLGdCQUFBLEdBQW1CLENBQUEsQ0FBRSxVQUFGO0VBQ25CLHFCQUFBLEdBQXdCLENBQUEsQ0FBRSxvQkFBRjtFQUN4QixPQUFBLEdBQVUsQ0FBQSxDQUFFLFVBQUY7RUFDVixVQUFBLEdBQWEsQ0FBQSxDQUFFLGNBQUY7RUFDYixVQUFBLEdBQWE7RUFDYixNQUFBLEdBQVM7RUFDVCxRQUFBLEdBQVc7RUFFWCxnQkFBQSxHQUFtQixTQUFBO0lBQ2pCLFVBQUEsQ0FBQTtJQUNBLFVBQUEsR0FBYTtFQUZJO0VBS25CLGdCQUFBLEdBQW1CLFNBQUE7SUFDakIsVUFBQSxDQUFBO0lBQ0EsVUFBQSxHQUFhO0VBRkk7RUFLbkIsU0FBQSxHQUFZLFNBQUMsQ0FBRCxFQUFJLENBQUo7SUFDVixJQUFHLENBQUEsSUFBSyxRQUFSO01BQ0Usb0JBQW9CLENBQUMsR0FBckIsQ0FDRTtRQUFBLE1BQUEsRUFBUSxDQUFSO09BREYsRUFERjtLQUFBLE1BQUE7TUFJRSxvQkFBb0IsQ0FBQyxHQUFyQixDQUNFO1FBQUEsTUFBQSxFQUFRLEdBQUEsR0FBTSxHQUFkO09BREYsRUFKRjs7RUFEVTtFQVVaLElBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFmLEtBQXdCLE9BQTNCO0lBRUUsV0FBQSxHQUFjO0lBRWQsWUFBQSxHQUNFO01BQUEsTUFBQSxFQUFRLEVBQVI7TUFDQSxNQUFBLEVBQVEsQ0FEUjtNQUVBLFNBQUEsRUFBVyxJQUZYO01BR0EsUUFBQSxFQUFVLElBSFY7O0lBS0YsVUFBQSxHQUFnQixDQUFBLFNBQUE7QUFFZCxVQUFBO01BQUEsSUFBQSxHQUFPLFNBQUMsT0FBRCxFQUFVLENBQVY7UUFDTCxJQUFHLENBQUEsSUFBSyxRQUFSO1VBQ0UsT0FBTyxDQUFDLE1BQVIsR0FBaUIsR0FEbkI7U0FBQSxNQUFBO1VBR0UsT0FBTyxDQUFDLE1BQVIsR0FBaUI7WUFBQSxRQUFBLEVBQVU7Y0FBQSxHQUFBLEVBQUssaURBQUw7YUFBVjtZQUhuQjs7UUFJQSxXQUFBLEdBQWMsTUFBTSxDQUFDLEtBQVAsQ0FBYSxPQUFiLEVBQXNCLE9BQXRCO1FBQ2QsV0FBVyxDQUFDLElBQVosQ0FBaUIsTUFBakIsRUFBeUIsU0FBQTtVQUN2QixXQUFXLENBQUMsS0FBWixDQUFBO1VBQ0EsV0FBVyxDQUFDLElBQVosQ0FBaUIsQ0FBakI7VUFDQSxXQUFXLENBQUMsSUFBWixDQUFBO2lCQUNBLElBQUMsQ0FBQTtRQUpzQixDQUF6QjtRQUtBLFdBQVcsQ0FBQyxJQUFaLENBQWlCLEtBQWpCLEVBQXdCLFNBQUE7VUFDdEIsV0FBVyxDQUFDLElBQVosQ0FBQTtRQURzQixDQUF4QjtNQVhLO01BZ0JQLElBQUEsQ0FBSyxZQUFMLEVBQW1CLENBQUEsQ0FBRSxNQUFGLENBQVMsQ0FBQyxLQUFWLENBQUEsQ0FBbkI7TUFFQSxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsRUFBVixDQUFhLFFBQWIsRUFBdUIsU0FBQTtRQUNyQixNQUFNLENBQUMsTUFBUCxDQUFBO1FBQ0EsSUFBQSxDQUFLLFlBQUwsRUFBbUIsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLEtBQVYsQ0FBQSxDQUFuQjtNQUZxQixDQUF2QjtJQXBCYyxDQUFBLENBQUgsQ0FBQTtJQTBCYixlQUFBLEdBQWtCLFNBQUE7TUFDaEIsV0FBVyxDQUFDLElBQVosQ0FBQTtJQURnQjtJQUlsQixlQUFBLEdBQWtCLFNBQUE7TUFDaEIsV0FBVyxDQUFDLEtBQVosQ0FBQTtJQURnQjtJQUlsQixPQUFBLEdBQVUsU0FBQTtNQUNSLENBQUEsQ0FBRSxNQUFGLENBQVMsQ0FBQyxFQUFWLENBQWEsUUFBYixFQUF1QixTQUFBO0FBQ3JCLFlBQUE7UUFBQSxLQUFBLEdBQVEsQ0FBQSxDQUFFLElBQUY7UUFDUixJQUFHLFVBQUEsS0FBYyxJQUFqQjtVQUNFLENBQUEsR0FBSSxvQkFBb0IsQ0FBQyxNQUFyQixDQUFBO1VBQ0osRUFBQSxHQUFLLEtBQUssQ0FBQyxTQUFOLENBQUEsQ0FBQSxHQUFvQixLQUFLLENBQUMsTUFBTixDQUFBLENBQUEsR0FBaUI7VUFDMUMsSUFBRyxDQUFBLEdBQUksRUFBUDtZQUNFLGVBQUEsQ0FBQSxFQURGO1dBQUEsTUFBQTtZQUdFLGVBQUEsQ0FBQSxFQUhGO1dBSEY7O01BRnFCLENBQXZCO0lBRFE7SUFhVixTQUFBLENBQVUsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLEtBQVYsQ0FBQSxDQUFWLEVBQTZCLENBQUEsQ0FBRSxNQUFGLENBQVMsQ0FBQyxNQUFWLENBQUEsQ0FBN0I7SUFDQSxPQUFBLENBQUE7SUFFQSxNQUFBLEdBQVM7TUFDUCxNQUFBLEVBQVEsU0FBQTtRQUNOLGVBQUEsQ0FBQTtRQUNBLGVBQWUsQ0FBQyxJQUFoQixDQUFBO1FBQ0EsZ0JBQWdCLENBQUMsUUFBakIsQ0FBMEIsTUFBMUI7UUFDQSxxQkFBcUIsQ0FBQyxRQUF0QixDQUErQixNQUEvQjtlQUNBLGdCQUFBLENBQUE7TUFMTSxDQUREO01BUVAsT0FBQSxFQUFTLFNBQUMsRUFBRDtRQUNQLENBQUEsQ0FBRSxFQUFGLENBQUssQ0FBQyxXQUFOLENBQWtCLE1BQWxCO1FBQ0EsZ0JBQUEsQ0FBQTtRQUNBLGVBQWUsQ0FBQyxJQUFoQixDQUFBO1FBQ0EsZ0JBQWdCLENBQUMsV0FBakIsQ0FBNkIsTUFBN0I7ZUFDQSxlQUFBLENBQUE7TUFMTyxDQVJGOztJQWdCVCxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsRUFBVixDQUFhLDBCQUFiLEVBQXlDLFNBQUE7YUFDdkMsT0FBQSxDQUFBO0lBRHVDLENBQXpDLEVBNUVGOztFQWdGQSxJQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBZixLQUF3QixPQUEzQjtJQUNFLE1BQUEsR0FBUztNQUNQLE1BQUEsRUFBUSxTQUFBO1FBQ04sZ0JBQWdCLENBQUMsUUFBakIsQ0FBMEIsTUFBMUI7UUFDQSxxQkFBcUIsQ0FBQyxRQUF0QixDQUErQixNQUEvQjtlQUNBLGdCQUFBLENBQUE7TUFITSxDQUREO01BTVAsT0FBQSxFQUFTLFNBQUMsRUFBRDtRQUNQLENBQUEsQ0FBRSxFQUFGLENBQUssQ0FBQyxXQUFOLENBQWtCLE1BQWxCO1FBQ0EsZ0JBQUEsQ0FBQTtlQUNBLGdCQUFnQixDQUFDLFdBQWpCLENBQTZCLE1BQTdCO01BSE8sQ0FORjtNQURYOztFQWFBLENBQUEsQ0FBRSxRQUFGLENBQ0UsQ0FBQyxFQURILENBQ00sa0JBRE4sRUFDMEIsVUFEMUIsRUFDc0MsU0FBQTtXQUNoQyxNQUFNLENBQUMsTUFBUCxDQUFBO0VBRGdDLENBRHRDO0VBSUEsQ0FBQSxDQUFFLFFBQUYsQ0FDRSxDQUFDLEVBREgsQ0FDTSxrQkFETixFQUMwQixvQkFEMUIsRUFDZ0QsU0FBQTtXQUM1QyxNQUFNLENBQUMsT0FBUCxDQUFlLENBQUEsQ0FBRSxJQUFGLENBQWY7RUFENEMsQ0FEaEQ7RUFJQSxDQUFBLENBQUUsTUFBRixDQUFTLENBQUMsRUFBVixDQUFhLDBCQUFiLEVBQXlDLFNBQUE7V0FDdkMsU0FBQSxDQUFVLENBQUEsQ0FBRSxJQUFGLENBQUksQ0FBQyxLQUFMLENBQUEsQ0FBVixFQUF3QixDQUFBLENBQUUsSUFBRixDQUFJLENBQUMsTUFBTCxDQUFBLENBQXhCO0VBRHVDLENBQXpDO0FBcElBLENBQUYiLCJmaWxlIjoidmlkZW9zY3JlZW4uanMifQ==
